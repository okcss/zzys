﻿using Common.Frame.Cacher;
using Common.Configure.Basics;
using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Common.Frame.Strainer
{
    /// <summary>
    /// 管理员登录过滤器
    /// </summary>
    public class Admin : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var actionName = filterContext.ActionDescriptor.ActionName;
            var menu = GlobalStatic._MenusConfig.Menus.FirstOrDefault(e => e.Href.EndsWith(actionName));

            filterContext.Controller.ViewBag.TabIndex = menu?.TabIndex;

            if (!GlobalStatic.AllowActions.Any(e => e.Equals(actionName, StringComparison.CurrentCultureIgnoreCase)))
            {
                try
                {
                    var currentManager = filterContext.HttpContext.Session[GlobalStatic.MANAGER_KEY] as SysManager;
                    if (currentManager == null)
                    {
                        filterContext.Result = CreateNotLoginResult(filterContext.HttpContext.Request);
                        return;
                    }

                    filterContext.Controller.ViewBag.SysManager = currentManager;
                    var roleMenus = SysCacher.ManagerPermissionsCache(currentManager.RoleId);
                    filterContext.Controller.ViewBag.RoleMenus = roleMenus;
                }
                catch (Exception)
                {
                    filterContext.Result = CreateNotLoginResult(filterContext.HttpContext.Request);
                    return;
                }

            }
            base.OnActionExecuting(filterContext);
        }
        private ActionResult CreateNotLoginResult(HttpRequestBase request)
        {
            if (request.IsAjaxRequest())
            {
                return new JResult
                {
                    ContentEncoding = Encoding.UTF8,
                    ContentType = "application/json",
                    Data = new { Error = true, Msg = "您还未登录或登录已失效,请重新登录.", IsLogin = true },
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet
                };
            }
            return request.HttpMethod.ToLower() == "post" ? new RedirectToRouteResult(new RouteValueDictionary(new { action = "SysLogin", controller = "Sys", from = request.UrlReferrer?.ToString() })) : new RedirectToRouteResult(new RouteValueDictionary(new { action = "SysLogin", controller = "Sys", from = request.Url?.ToString() }));
        }
    }
}
