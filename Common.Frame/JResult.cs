﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace Common.Frame
{
    #region 重写JSONResult返回类型,避免序列化和反序列化时循环引用 
    /// <inheritdoc />
    /// <summary>
    /// 重写JSONResult返回类型,
    /// 避免序列化和反序列化时循环引用
    /// 避免MVC.JsonResult不能反序列化字典类型
    /// </summary>
    public class JResult : JsonResult
    {
        /// <inheritdoc />
        /// <summary>
        /// 重写JSONResult返回类型,
        /// 避免序列化和反序列化时循环引用
        /// 避免MVC.JsonResult不能反序列化字典类型
        /// </summary>
        public JResult()
        {
            MaxJsonLength = 1024000;
            RecursionLimit = 50;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("当前请求上下文为空.");
            }
            if (JsonRequestBehavior == JsonRequestBehavior.DenyGet &&
                string.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("当前请求不允许以Get方式获取.");
            }
            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = string.IsNullOrEmpty(ContentType) ? "application/json" : ContentType;
            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }
            if (Data == null)
            {
                Data = new { Error = true, Msg = "未请求到数据." };
            }

            var scriptSerializer = JsonSerializer.Create(GlobalStatic.SerializerSettings);

            using (var sw = new StringWriter())
            {
                scriptSerializer.Serialize(sw, Data);

                response.Write(sw.ToString());
            }
        }
    }
    #endregion
}
