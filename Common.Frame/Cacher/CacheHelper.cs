﻿using Common.Frame;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;

namespace Common.Frame.Cacher
{
    public abstract class CacheHelper
    {
        #region Private

        protected static readonly object _Lock = new object();
        protected static readonly List<string> _CacheKeys = new List<string>();
        /// <summary>
        /// 缓存相对更新时间
        /// </summary>
        protected static readonly int SlidingInterval = GlobalStatic._GlobalConfig.SlidingInterval;
        /// <summary>
        /// 绝对缓存更新时间
        /// </summary>
        protected static readonly int AbsoluteInterval = GlobalStatic._GlobalConfig.AbsoluteInterval;
        /// <summary>
        /// 插入相对缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        protected static void InsertSlidingCache(string key, object value)
        {
            if (_CacheKeys.Contains(key))
            {
                HttpContext.Current.Cache.Insert(key, value, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, SlidingInterval, 0), CacheItemPriority.Default, null);
            }
            else
            {
                lock (_Lock)
                {
                    _CacheKeys.Add(key);
                }
                HttpContext.Current.Cache.Add(key, value, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, SlidingInterval, 0), CacheItemPriority.Default, null);
            }
        }
        /// <summary>
        /// 插入绝对缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void InsertAbsoluteCache(string key, object value)
        {
            if (_CacheKeys.Contains(key))
            {
                HttpContext.Current.Cache.Add(key,
                                              value,
                                              null,
                                              DateTime.Now.AddMinutes(AbsoluteInterval),
                                              Cache.NoSlidingExpiration,
                                              CacheItemPriority.Default,
                                              RemovedCallback);
            }
            else
            {
                lock (_Lock)
                {
                    _CacheKeys.Add(key);
                }
                HttpContext.Current.Cache.Add(key,
                                              value,
                                              null,
                                              DateTime.Now.AddMinutes(AbsoluteInterval),
                                              Cache.NoSlidingExpiration,
                                              CacheItemPriority.Default,
                                              RemovedCallback);
            }
        }
        /// <summary>
        /// 删除指定key的缓存对象
        /// </summary>
        /// <param name="key"></param>
        public static void RemoveCache(string key)
        {
            HttpContext.Current.Cache.Remove(key);
            lock (_Lock)
            {
                _CacheKeys.Remove(key);
            }
        }

        /// <summary>
        /// 移除所有缓存数据。
        /// </summary>
        public static void RemoveAll()
        {
            lock (_CacheKeys)
            {
                foreach (var t in _CacheKeys)
                {
                    HttpContext.Current.Cache.Remove(t);
                }

                _CacheKeys.Clear();
            }
        }

        /// <summary>
        /// 获取当前系统缓存中所有Key。
        /// </summary>
        public static IEnumerable<string> Keys
        {
            get
            {
                lock (_CacheKeys)
                {
                    foreach (var item in _CacheKeys)
                    {
                        yield return item;
                    }
                }
            }
        }

        /// <summary>
        /// 缓存项删除时回调
        /// </summary>
        protected static CacheItemRemovedCallback RemovedCallback
        {
            get
            {
                return (key, value, reason) =>
                {
                    if (reason == CacheItemRemovedReason.Removed) return;
                    lock (_Lock)
                    {
                        _CacheKeys.Remove(key);
                    }
                };
            }
        }


        /// <summary>
        /// 获取指定缓存中key的值
        /// </summary>
        public static object GetGlobalKeyValue(string key)
        {
            return HttpContext.Current.Cache.Get(key);
        }
        /// <summary>
        /// 删除指定缓存
        /// </summary>
        /// <param name="key"></param>
        public static void RemoveCacheKey(string key)
        {
            HttpContext.Current.Cache.Remove(key);
        }
        /// <summary>
        /// 插入不可变更的绝对缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void InsertNotOperableClearAbsoluteCache(string key, object value)
        {
            HttpContext.Current.Cache.Add(key,
                                          value,
                                          null,
                                          DateTime.Now.AddMinutes(AbsoluteInterval),
                                          Cache.NoSlidingExpiration,
                                          CacheItemPriority.Default,
                                          RemovedCallback);
        }

        #endregion
    }
}
