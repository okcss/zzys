﻿using Common.Configure.Basics;
using Common.Configure.Supports;
using Common.Frame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Common.Frame.Cacher
{
    public class SysCacher : CacheHelper
    {
        #region 权限管理缓存

        /// <summary>
        /// 所有菜单列表缓存
        /// </summary>
        public static List<SysMenu> SystemMenuListCache
        {
            get
            {
                var list = HttpContext.Current.Cache[GlobalStatic.SYSTEM_MENUS] as List<SysMenu>;
                if (list == null)
                {
                    list = GlobalStatic._MenusConfig.Menus.Where(e => e.State == ConfigEnums.AnyStatus.Valid)

                                       .OrderBy(e => e.Sort).ToList();

                    InsertSlidingCache(GlobalStatic.SYSTEM_MENUS, list);
                }
                return list;
            }
        }
        /// <summary>
        /// 根据管理员角色Id获取管理员权限列表缓存
        /// </summary>
        /// <param name="roleId">角色编号Guid</param>
        /// <returns></returns>
        public static List<SysMenu> ManagerPermissionsCache(Guid roleId)
        {
            var key = string.Format(GlobalStatic.ROLE_PERMISSIONS, roleId);
            var obj = HttpContext.Current.Cache.Get(key) as List<SysMenu>;
            if (obj == null)
            {
                obj = GlobalStatic._PermissionsConfig.Permissions
                                  .Where(e => e.RoleId == roleId && e.State == ConfigEnums.AnyStatus.Valid)
                                  .Select(e => e.SysMenu)
                                  .OrderBy(e => e?.Sort).ToList();

                InsertSlidingCache(key, obj);
            }
            return obj;
        }

        #endregion

        #region 角色缓存
        /// <summary>
        /// 所有角色的缓存
        /// </summary>
        public static List<SysRole> RoleCache
        {
            get
            {
                List<SysRole> list = HttpContext.Current.Cache[GlobalStatic.ROLE_LIST] as List<SysRole>;
                if (list == null)
                {
                    list = GlobalStatic._RolesConfig.Roles.Where(e => e.State == ConfigEnums.AnyStatus.Valid)
                                       .OrderBy(e => e.Sort).ToList();
                    InsertSlidingCache(GlobalStatic.ROLE_LIST, list);
                }
                return list;
            }
        }

        #endregion


        
    }
}
