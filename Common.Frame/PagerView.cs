﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Frame
{
    /// <summary>
    /// Ajax泛型分页
    /// <para>调用方法:</para>
    /// <para>1.linq查询数据并调用OrderBy排序.var query = entities.T.where().orderby();</para>
    /// <para>2.var result = new PagerView&lt;T&gt;(页数)</para>
    /// <para>3.调用 result.Pager(query); </para>
    /// <para>4.result就是要返回的当前页数据</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class PagerView<T>
    {
        /// <summary>
        /// 泛型分页的构造函数
        /// <para>调用方法:</para>
        /// <para>1.linq查询数据并调用OrderBy排序.var query = entities.T.where().orderby();</para>
        /// <para>2.var result = new PagerView&lt;T&gt;(页数)</para>
        /// <para>3.调用 result.Pager(query); </para>
        /// <para>4.result就是要返回的当前页数据</para>
        /// </summary>
        /// <param name="index">第几页</param>
        public PagerView(int index = 1)
        {
            PageSize = GlobalStatic._GlobalConfig.DefaultPageSize <= 0 ? 20 : GlobalStatic._GlobalConfig.DefaultPageSize;
            Index = index;
            PagerData = new List<T>();
        }
        public int Index { get; set; }
        public int ShowPagerCount
        {
            get
            {
                return 10;
            }
        }
        public int DataCount { get; set; }
        public int PageCount
        {
            get
            {
                return (DataCount % PageSize != 0) ? DataCount / PageSize + 1 : DataCount / PageSize;
            }
        }
        public int PageSize { get; set; }
        public IList<T> PagerData { get; set; }

        /// <summary>
        /// 分页函数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public void Pager(IOrderedQueryable<T> model)
        {
            DataCount = model.Count();
            PagerData = model.Skip((Index - 1) * PageSize)
                        .Take(PageSize)
                        .ToList();
        }
        
    }
}
