﻿using Common.Configure.Basics;
using Common.Frame.Strainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Frame.Basic
{
    [Admin]
    public class AdminBaseController : BaseController
    {
        protected SysManager CurrentManager
        {
            get
            {
                return Session[GlobalStatic.MANAGER_KEY] as SysManager;
            }
        }
    }
}
