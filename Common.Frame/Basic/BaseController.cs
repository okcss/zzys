﻿using Common.Frame.Strainer;
using Common.Configure.Basics;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Web.Mvc;

namespace Common.Frame.Basic
{
    public class BaseController : Controller
    {

        protected new JsonResult Json(object data, JsonRequestBehavior allowGet = JsonRequestBehavior.DenyGet)
        {
            return new JResult
            {
                ContentEncoding = Encoding.UTF8,
                ContentType = "application/json",
                JsonRequestBehavior = allowGet,
                Data = data
            };
        }

        protected string SerializeObject(object value)
        {
            try
            {
                return JsonConvert.SerializeObject(value, GlobalStatic.SerializerSettings);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// 获取web客户端ip
        /// </summary>
        /// <returns></returns>
        protected string ClientIP
        {
            get
            {
                #region 
                string clientIP = "未获取用户IP";
                try
                {
                    if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Request == null || System.Web.HttpContext.Current.Request.ServerVariables == null) { return "Client Error"; }


                    clientIP = string.Empty;

                    //CDN加速后取到的IP simone 090805
                    clientIP = System.Web.HttpContext.Current.Request.Headers["Cdn-Src-Ip"];
                    if (!string.IsNullOrEmpty(clientIP))
                    {
                        return clientIP;
                    }

                    clientIP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                    if (!string.IsNullOrEmpty(clientIP))
                    {
                        return clientIP;
                    }

                    if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        clientIP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                        if (string.IsNullOrWhiteSpace(clientIP))
                        {
                            clientIP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                        }
                    }
                    else
                    {
                        clientIP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                    }

                    if (string.Compare(clientIP, "unknown", true) == 0 || string.IsNullOrEmpty(clientIP))
                    {
                        return System.Web.HttpContext.Current.Request.UserHostAddress;
                    }
                    return clientIP;
                }
                catch
                {
                    return "catch exception";
                }
                #endregion
            }
        }
    }
}
