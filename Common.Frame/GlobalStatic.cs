﻿using Common.Configure.Configs;
using Newtonsoft.Json;
using System;
using Common.Configure.Supports;

namespace Common.Frame
{
    /// <summary>
    /// 应用程序全局配置
    /// </summary>
    public static partial class GlobalStatic
    {
        /// <summary>
        /// 构造函数
        /// <para>初始化各配置文件对象</para>
        /// <para>初始化Json.net反序列化时配置</para>
        /// </summary>
        static GlobalStatic()
        {
            _AdminConfig = ConfigSingle<AdminsConfig>.Instance;
            _GlobalConfig = ConfigSingle<GlobalConfig>.Instance;
            _MenusConfig = ConfigSingle<MenusConfig>.Instance;
            _PermissionsConfig = ConfigSingle<PermissionsConfig>.Instance;
            _RolesConfig = ConfigSingle<RolesConfig>.Instance;

            SerializerSettings = new JsonSerializerSettings
            {
                //循环引用json.net官方给出的解决配置选项.
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                //解决忽略某些属性后导致序列化属性不匹配
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

        }
    }
    /// <summary>
    /// 配置文件对象
    /// </summary>
    public static partial class GlobalStatic
    {
        /// <summary>
        /// 管理员配置
        /// </summary>
        public static readonly AdminsConfig _AdminConfig;
        /// <summary>
        /// 基础配置
        /// </summary>
        public static readonly GlobalConfig _GlobalConfig;
        /// <summary>
        /// 菜单列表配置
        /// </summary>
        public static readonly MenusConfig _MenusConfig;
        /// <summary>
        /// 权限配置对象
        /// </summary>
        public static readonly PermissionsConfig _PermissionsConfig;
        /// <summary>
        /// 角色配置
        /// </summary>
        public static readonly RolesConfig _RolesConfig;
    }
    /// <summary>
    /// 全局静态变量
    /// </summary>
    public static partial class GlobalStatic
    {
        public static JsonSerializerSettings SerializerSettings { get; private set; }

        /// <summary>
        /// 未登录时允许访问的Actions地址
        /// </summary>
        public static string[] AllowActions = new string[] {
            "SysLogin","SendSmsCode","ImageCode"
        };

        /// <summary>
        /// 默认的权限父节点ID,顶级菜单的父节点
        /// </summary>
        public static readonly Guid DefaultParentId = Guid.Parse("88888888-8888-8888-8888-888888888888");

    }

    /// <summary>
    /// 常量
    /// </summary>
    public static partial class GlobalStatic
    {
        public const string MANAGER_KEY = "manager_session_key";
        public const string MANAGER_LOGIN_CODE_KEY = "sys_login_code";
        #region 角色缓存
        /// <summary>
        /// 角色缓存key
        /// </summary>
        public const string ROLE_LIST = "Role_List";
        #endregion

        #region 权限页面
        /// <summary>
        /// 权限页面缓存key
        /// </summary>
        public const string SYSTEM_MENUS = "SystemMenu";

        /// <summary>
        /// 角色权限缓存key
        /// </summary>
        public const string ROLE_PERMISSIONS = "{0}_Permissions";

        #endregion
    }


}
