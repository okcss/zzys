﻿using System;

namespace Common.Frame.Security
{
    public static class GIdentity
    {
        #region Private Property
        private static long _WorkerId = 0;
        private static long _DataCenterId = 0;
        private static long _Sequence;
        private static long _Twepoch = 1288834974657L;
        private static long _WorkerIdBits = 5L;
        private static long _DatacenterIdBits = 5L;
        //-1L ^ (-1L <<< (int)_WorkerIdBits) = 63;
        /// <summary>
        /// _WorkerIdBits=  5L;
        /// </summary>
        //private static long _MaxWorkerId = -1L ^ (-1L << (int)_WorkerIdBits);
        //private static long _MaxDatacenterId = -1L ^ (-1L << (int)_DatacenterIdBits);
        private static long _SequenceBits = 12L;

        private static readonly long _WorkerIdShift = _SequenceBits;
        private static readonly long _DataCenterIdShift = _SequenceBits + _WorkerIdBits;
        private static readonly long _TimestampLeftShift = _SequenceBits + _WorkerIdBits + _DatacenterIdBits;
        private static readonly long _SequenceMask = -1L ^ (-1L << (int)_SequenceBits);

        private static long _LastTimestamp = -1L;
        private static readonly object _SyncRoot = new object();
        #endregion

        #region Public Property
        public static long NewId
        {
            get
            {
                return GetIdentity();
            }
        }
        #endregion

        #region GetIdentity
        private static long GetIdentity()
        {
            lock (_SyncRoot)
            {
                long timestamp = TimeGen();

                if (timestamp < _LastTimestamp)
                {
                    throw new ApplicationException($"Clock moved backwards.  Refusing to generate id for {_LastTimestamp - timestamp} milliseconds");
                }

                if (_LastTimestamp == timestamp)
                {
                    _Sequence = (_Sequence + 1) & _SequenceMask;
                    if (_Sequence == 0)
                    {
                        timestamp = TilNextMillis(_LastTimestamp);
                    }
                }
                else
                {
                    _Sequence = 0L;
                }

                _LastTimestamp = timestamp;

                return ((timestamp - _Twepoch) << (int)_TimestampLeftShift) | (_DataCenterId << (int)_DataCenterIdShift) | (_WorkerId << (int)_WorkerIdShift) | _Sequence;
            }
        }
        #endregion

        #region PrivateMethods
        private static long TilNextMillis(long lastTimestamp)
        {
            long timestamp = TimeGen();
            while (timestamp <= lastTimestamp)
            {
                timestamp = TimeGen();
            }
            return timestamp;
        }

        private static long TimeGen()
        {
            return (long)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
        }
        #endregion
    }

}
