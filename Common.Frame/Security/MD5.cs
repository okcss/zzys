﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Common.Frame.Security
{
    /// <summary>
    /// 提供MD5字符串加密。
    /// </summary>
    public static class MD5
    {
        /// <summary>
        /// 加密字符串
        /// </summary>
        /// <param name="sourceString"></param>
        /// <returns></returns>
        public static string Encrypt(string sourceString)
        {
            if (string.IsNullOrEmpty(sourceString))
            {
                throw new Exception("<Encrypt Error>sourceString is null");
            }

            var md5 = new MD5CryptoServiceProvider();
            var buffer = md5.ComputeHash(Encoding.UTF8.GetBytes(sourceString));
            var sb = new StringBuilder(32);
            foreach (var t in buffer)
            {
                sb.Append(t.ToString("X2"));
            }

            md5.Dispose();
            return sb.ToString();
        }

        /// <summary>
        /// 16位MD5加密
        /// </summary>
        /// <param name="sourceString"></param>
        /// <returns></returns>
        public static string Encrypt16(string sourceString)
        {
            var md5 = new MD5CryptoServiceProvider();
            var t2 = BitConverter.ToString(md5.ComputeHash(Encoding.Default.GetBytes(sourceString)), 4, 8);
            t2 = t2.Replace("-", "");
            return t2;
        }
    }
}
