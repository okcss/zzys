﻿using Common.Configure.Basics;
using System;

namespace Common.Frame.Security
{
    public static class Password
    {
        /// <summary>
        /// 加密管理员登陆密码
        /// </summary>
        /// <param name="administrator">管理员对象</param>
        /// <returns></returns>
        public static string ManagerEncode(SysManager administrator)
        {
            if (administrator == null) throw new ArgumentNullException(nameof(administrator), "要计算的(admin)对象是空值.p1");
            if (administrator.SysCode == Guid.Empty || string.IsNullOrWhiteSpace(administrator.Password)) throw new ArgumentNullException(nameof(administrator), "要计算的(admin)对象是空值.p2");
            return ManagerEncode(administrator.SysCode, administrator.Password); 
        }

        public static string ManagerEncode(Guid sysCode, string sourcePassword)
        {
            if (sysCode == Guid.Empty || string.IsNullOrWhiteSpace(sourcePassword)) throw new ArgumentNullException(nameof(sysCode), "要计算的(admin)对象是空值.");
            return MD5.Encrypt(MD5.Encrypt(sourcePassword) + MD5.Encrypt(sysCode.ToString("N")));
        }

        /// <summary>
        /// 匹配管理员密码是否相同
        /// </summary>
        /// <param name="dbPwd">数据库中的密码</param>
        /// <param name="inputPwd">前台输入的密码</param>
        /// <param name="sysCode">管理员标识</param>
        /// <returns></returns>
        public static bool IsManagerPassword(string dbPwd, string inputPwd, Guid sysCode)
        {
            var encode = MD5.Encrypt(MD5.Encrypt(inputPwd) + MD5.Encrypt(sysCode.ToString("n")));
            return encode.Equals(dbPwd);
        }
    }
}
