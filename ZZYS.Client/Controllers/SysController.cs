﻿using Common.Configure.Basics;
using Common.Configure.Configs;
using Common.Frame;
using Common.Frame.Basic;
using Common.Frame.Cacher;
using Common.Frame.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Common.Configure.Supports.ConfigEnums;

namespace ZZYS.Client.Controllers
{
    public class SysController : BaseController
    {
        #region 管理员登录
        [HttpGet]
        public ActionResult SysLogin()
        {
            return View();
        }
        [HttpPost]
        public JsonResult SysLogin(string phone, string pwd, string code)
        {
            if (string.IsNullOrWhiteSpace(phone)) { return Json(new { Error = true, Msg = "缺少必要参数,管理员登录账号." }); }
            if (string.IsNullOrWhiteSpace(pwd)) { return Json(new { Error = true, Msg = "缺少必要参数,管理员登录密码." }); }
            if (string.IsNullOrWhiteSpace(code)) { return Json(new { Error = true, Msg = "缺少必要参数,验证码." }); }
            var sessionCode = Session[GlobalStatic.MANAGER_LOGIN_CODE_KEY] as string;
            if (string.IsNullOrWhiteSpace(sessionCode)) { return Json(new { Error = true, Msg = "未获取到服务器验证码，请刷新页面后重试." }); }
            if (!string.Equals(sessionCode, code, StringComparison.CurrentCultureIgnoreCase)) { return Json(new { Error = true, Msg = "验证码不正确." }); }
            Session.Remove(GlobalStatic.MANAGER_LOGIN_CODE_KEY);
            var adminsConfig = GlobalStatic._AdminConfig;
            if (adminsConfig?.Administrators == null || !adminsConfig.Administrators.Any())
            {
                return Json(new { Error = true, Msg = "未获取到管理员数据,请联系技术人员." });
            }
            var adm = adminsConfig.Administrators.FirstOrDefault(e => e.Phone == phone);
            if (adm == null)
            {
                return Json(new { Error = true, Msg = "管理员账号不正确." });
            }
            if (adm.State != AnyStatus.Valid)
            {
                return Json(new { Error = true, Msg = "该账户暂无法登录." });
            }
            var isValid = Password.IsManagerPassword(adm.Password, pwd, adm.SysCode);
            if (isValid)
            {
                try
                {
                    adm.LoginIP = ClientIP;
                    adm.LastLoginTime = DateTime.Now;
                    adminsConfig.SaveConfig();
                    Session[GlobalStatic.MANAGER_KEY] = adm;
                    return Json(new { Error = false });
                }
                catch (Exception)
                {
                    //Log:
                    return Json(new { Error = true, Msg = "登录异常,请联系管理员查看服务器日志." });
                }
            }
            else
            {
                return Json(new { Error = true, Msg = "密码校验不正确." });
            }
        }
        #endregion

        #region 权限管理
        #region 管理员信息管理
        #region 管理员列表
        [HttpGet]
        public ActionResult Admins()
        {
            return View();
        }
        #endregion
        #region 修改管理员信息
        [HttpGet]
        public ActionResult ModifyAdmin(Guid id)
        {
            return View();
        }
        [HttpPost]
        public JsonResult ModifyAdmin(SysManager admin)
        {
            return Json(new { Error = false });
        }
        #endregion
        #region 修改管理员状态
        [HttpPost]
        public JsonResult SetAdminState(Guid id, AnyStatus state)
        {
            return Json(new { Error = false });
        }
        #endregion
        #region 修改管理员密码
        [HttpGet]
        public ActionResult ModifyPassword()
        {
            return View();
        }
        [HttpPost]
        public JsonResult ModifyPassword(string oldPwd, string newPwd, string smsCode)
        {
            return Json(new { Error = false });
        }
        #endregion
        #region 删除管理员
        [HttpPost]
        public JsonResult RemoveAdmin(Guid id)
        {
            return Json(new { Error = false });
        }
        #endregion
        #region 添加管理员
        [HttpGet]
        public ActionResult NewAdmin()
        {
            return View();
        }
        [HttpPost]
        public JsonResult NewAdmin(SysManager admin)
        {
            return Json(new { Error = false });
        }
        #endregion
        #endregion
        #region 角色管理
        #region 角色列表
        [HttpGet]
        public ActionResult Roles()
        {
            return View();
        }
        #endregion
        #region 添加角色
        [HttpGet]
        public ActionResult NewRole()
        {
            return View();
        }
        [HttpPost]
        public JsonResult NewRole(SysRole role)
        {
            return Json(new { Error = false });
        }
        #endregion
        #region 删除角色
        [HttpPost]
        public JsonResult RemoveRole(Guid id)
        {
            return Json(new { Error = false });
        }
        #endregion
        #region 修改角色状态
        [HttpPost]
        public JsonResult SetRoleState(Guid id, AnyStatus state)
        {
            return Json(new { Error = false });
        }
        #endregion
        #region 编辑角色权限
        [HttpGet]
        public ActionResult ModifyRole(Guid id)
        {
            return View();
        }
        [HttpPost]
        public JsonResult ModifyRole(Guid id, List<Guid> menus)
        {
            return Json(new { Error = false });
        }
        #endregion
        #endregion
        #region 权限菜单管理
        #region 菜单列表
        [HttpGet]
        public ActionResult Menus()
        {
            return View();
        }

        /// <summary>
        /// ajax加载菜单列表
        /// </summary>
        /// <param name="p">父节点编号</param>
        /// <param name="n">节点名称</param>
        /// <param name="isPager">是否分页</param>
        /// <param name="idx">页码</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LoadMenus(Guid? p, string n, bool isPager = true, int idx = 1)
        {
            try
            {
                var allMenus = SysCacher.SystemMenuListCache.AsQueryable();

                if (p.HasValue && p.Value != Guid.Empty)
                {
                    allMenus = allMenus.Where(e => e.ParentId == p.Value);
                }
                if (!string.IsNullOrWhiteSpace(n))
                {
                    allMenus = allMenus.Where(e => e.DisplayName.Contains(n));
                }
                var clone = allMenus.Select(e => new {
                    e.DisplayName,
                    e.Href,
                    e.IsDisplay,
                    e.ICON,
                    e.TabIndex,
                    e.ParentName,
                    e.Id,
                    e.Sort
                });
                if (!isPager) return Json(new { Error = false, Data = clone });


                var tempQuery = allMenus.Select(e => new {
                    e.DisplayName,
                    e.Href,
                    e.IsDisplay,
                    e.ICON,
                    e.TabIndex,
                    e.ParentName,
                    e.Id,
                    e.Sort
                }).OrderBy(e => e.Sort);
                var result = new PagerView<dynamic>(idx);
                result.Pager(tempQuery);
                return Json(new { Error = false, Data = result });

            }
            catch (Exception)
            {
                return Json(new { Error = true, Msg = "抱歉服务器异常。LoadMenus Error" });
            }
        }
        #endregion
        #region 添加菜单
        [HttpGet]
        public ActionResult NewMenu()
        {
            return View();
        }
        [HttpPost]
        public JsonResult NewMenu(SysMenu menu)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(menu.Href))
                {
                    return Json(new { Error = true, Msg = "菜单链接地址必须填写。" });
                }
                if (string.IsNullOrWhiteSpace(menu.DisplayName))
                {
                    return Json(new { Error = true, Msg = "菜单名称必须填写。" });
                }
                var isExis = SysCacher.SystemMenuListCache.Any(e => e.DisplayName.Equals(menu.DisplayName, StringComparison.CurrentCultureIgnoreCase));
                if (isExis)
                {
                    return Json(new { Error = true, Msg = "菜单名称已存在，不能重复添加。" });
                }
                if (menu.MenuLevel == MenuLevel.None)
                {
                    return Json(new { Error = true, Msg = "必须选择菜单所属级别。" });
                }

                menu.Id = Guid.NewGuid();
                menu.State = AnyStatus.Valid;
                menu.TabIndex = $"{GIdentity.NewId}";
                //计算顶级菜单的 Sort
                var topMenuCount = SysCacher.SystemMenuListCache.Count(e => e.ParentId == GlobalStatic.DefaultParentId);
                menu.Sort = topMenuCount + 1;

                //菜单级别,如果是 顶级菜单
                if (menu.MenuLevel == MenuLevel.Top)
                {
                    if (string.IsNullOrWhiteSpace(menu.ICON))
                    {
                        menu.ICON = "el-icon-menu";
                    }
                    menu.ParentId = GlobalStatic.DefaultParentId;
                }
                else
                {
                    #region 如果不是顶级菜单,则判断所属的父节点编号是否有效
                    if (menu.ParentId == Guid.Empty || menu.ParentId == GlobalStatic.DefaultParentId)
                    {
                        return Json(new { Error = true, Msg = "非顶级菜单，必须选择所属上级菜单。" });
                    }

                    var pMenu = SysCacher.SystemMenuListCache.FirstOrDefault(e => e.Id == menu.ParentId);
                    if (pMenu == null)
                    {
                        return Json(new { Error = true, Msg = "未查询到所选的上级菜单，请刷新页面重新尝试。" });
                    }
                    #endregion

                }
                var newMenu = new SysMenu()
                {
                    Id = menu.Id,//
                    DisplayName = menu.DisplayName,//
                    MenuLevel = menu.MenuLevel,//
                    Href = menu.Href,//
                    ICON = menu.ICON,//
                    IsDisplay = menu.IsDisplay,
                    ParentId = menu.ParentId, //
                    Sort = menu.Sort,
                    State = menu.State,//
                    TabIndex = menu.TabIndex //
                };

                GlobalStatic._MenusConfig.Menus.Add(newMenu);
                GlobalStatic._MenusConfig.SaveConfig();

                CacheHelper.RemoveCache(GlobalStatic.SYSTEM_MENUS);
                return Json(new { Error = false });
            }
            catch (Exception)
            {
                return Json(new { Error = true, Msg = "抱歉服务器异常。NewMenu Error" });
            }
        }
        /// <summary>
        /// 判断菜单的TabIndex是否已存在
        /// TabIndex用于菜单的选中事件，唯一
        /// </summary>
        /// <param name="actName"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult TabIndexIsExis(string tabIndex)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(tabIndex))
                {
                    return Json(new { Error = true, Msg = "缺少参数:菜单唯一标识!" });
                }
                var isExis = SysCacher.SystemMenuListCache.Any(e => e.TabIndex == tabIndex);
                return Json(new { Error = false, isExis });
            }
            catch (Exception)
            {
                return Json(new { Error = true, Msg = "抱歉服务器异常。MenuActionIsExis Error" });
            }
        }
        [HttpPost]
        public JsonResult MenuNameIsExis(string menuName)
        {
            try
            {
                var isExis = SysCacher.SystemMenuListCache.Any(e => e.DisplayName.Equals(menuName, StringComparison.CurrentCultureIgnoreCase));
                return Json(new { Error = false, isExis = isExis });
            }
            catch (Exception)
            {
                return Json(new { Error = true, Msg = "抱歉服务器异常。MenuActionIsExis Error" });
            }
        }
        #endregion
        #region 删除菜单
        [HttpPost]
        public JsonResult RemoveMenu(SysMenu menu)
        {
            return Json(new { Error = false });
        }
        #endregion
        #region 编辑菜单信息
        [HttpGet]
        public ActionResult ModifyMenu(Guid id)
        {
            return View();
        }
        [HttpPost]
        public JsonResult ModifyMenu(SysMenu newMenu)
        {
            return Json(new { Error = false });
        }
        #endregion
        #endregion
        #endregion

        #region 配置文件管理
        #region 全局配置
        [HttpGet]
        public ActionResult GlobalConfig()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GlobalConfig(GlobalConfig global)
        {
            return Json(new { Error = false });
        }
        #endregion

        #endregion

        #region 缓存清理
        [HttpGet]
        public ActionResult ClearCache()
        {
            return View();
        }
        [HttpPost]
        public JsonResult ClearCache(string keys)
        {
            return Json(new { Error = false });
        }
        #endregion

        #region 短信验证码
        /// <summary>
        /// 发送短信验证码
        /// </summary>
        /// <param name="smsType">验证码类型</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SendSmsCode(string smsType)
        {
            return Json(new { Error = false });
        }
        #endregion

        #region 图形验证码
        [HttpGet]
        public FileResult ImageCode()
        {
            var img = new VerifyImager();
            var randCode = img.GetRandomString(4);
            var result = img.CreateImage(randCode);
            Session[GlobalStatic.MANAGER_LOGIN_CODE_KEY] = randCode;
            return File(result, "image/jpeg");
        }
        #endregion
    }
}