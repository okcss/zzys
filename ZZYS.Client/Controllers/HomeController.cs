﻿using Common.Frame.Basic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZZYS.Client.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Client
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Default()
        {
            return View();
        }
    }
}