﻿/// <reference path="../plugins.js" />
(function () {

    var width, height, largeHeader, canvas, ctx, points, target, animateHeader = true;

    // Main
    initHeader();
    initAnimation();
    addListeners();

    function initHeader() {
        width = window.innerWidth;
        height = window.innerHeight;
        target = { x: width / 2, y: height / 2 };

        largeHeader = document.getElementById('larger-header');
        largeHeader.style.height = height + 'px';

        canvas = document.getElementById('login-canvas');
        canvas.width = width;
        canvas.height = height;
        ctx = canvas.getContext('2d');

        // create points
        points = [];
        for (var x = 0; x < width; x = x + width / 20) {
            for (var y = 0; y < height; y = y + height / 20) {
                var px = x + Math.random() * width / 20;
                var py = y + Math.random() * height / 20;
                var p = { x: px, originX: px, y: py, originY: py };
                points.push(p);
            }
        }

        // for each point find the 5 closest points
        for (var i = 0; i < points.length; i++) {
            var closest = [];
            var p1 = points[i];
            for (var j = 0; j < points.length; j++) {
                var p2 = points[j]
                if (!(p1 == p2)) {
                    var placed = false;
                    for (var k = 0; k < 5; k++) {
                        if (!placed) {
                            if (closest[k] == undefined) {
                                closest[k] = p2;
                                placed = true;
                            }
                        }
                    }

                    for (var k = 0; k < 5; k++) {
                        if (!placed) {
                            if (getDistance(p1, p2) < getDistance(p1, closest[k])) {
                                closest[k] = p2;
                                placed = true;
                            }
                        }
                    }
                }
            }
            p1.closest = closest;
        }

        // assign a circle to each point
        for (var i in points) {
            var c = new Circle(points[i], 2 + Math.random() * 2, 'rgba(255,255,255,0.3)');
            points[i].circle = c;
        }
    }

    // Event handling
    function addListeners() {
        if (!('ontouchstart' in window)) {
            window.addEventListener('mousemove', mouseMove);
        }
        window.addEventListener('scroll', scrollCheck);
        window.addEventListener('resize', resize);
    }

    function mouseMove(e) {
        var posx = posy = 0;
        if (e.pageX || e.pageY) {
            posx = e.pageX;
            posy = e.pageY;
        }
        else if (e.clientX || e.clientY) {
            posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        }
        target.x = posx;
        target.y = posy;
    }

    function scrollCheck() {
        if (document.body.scrollTop > height) animateHeader = false;
        else animateHeader = true;
    }

    function resize() {
        width = window.innerWidth;
        height = window.innerHeight;
        largeHeader.style.height = height + 'px';
        canvas.width = width;
        canvas.height = height;
    }

    // animation
    function initAnimation() {
        animate();
        for (var i in points) {
            shiftPoint(points[i]);
        }
    }

    function animate() {
        if (animateHeader) {
            ctx.clearRect(0, 0, width, height);
            for (var i in points) {
                // detect points in range
                if (Math.abs(getDistance(target, points[i])) < 4000) {
                    points[i].active = 0.3;
                    points[i].circle.active = 0.6;
                } else if (Math.abs(getDistance(target, points[i])) < 20000) {
                    points[i].active = 0.1;
                    points[i].circle.active = 0.3;
                } else if (Math.abs(getDistance(target, points[i])) < 40000) {
                    points[i].active = 0.02;
                    points[i].circle.active = 0.1;
                } else {
                    points[i].active = 0;
                    points[i].circle.active = 0;
                }

                drawLines(points[i]);
                points[i].circle.draw();
            }
        }
        requestAnimationFrame(animate);
    }

    function shiftPoint(p) {
        TweenLite.to(p, 1 + 1 * Math.random(), {
            x: p.originX - 50 + Math.random() * 100,
            y: p.originY - 50 + Math.random() * 100, ease: Circ.easeInOut,
            onComplete: function () {
                shiftPoint(p);
            }
        });
    }

    // Canvas manipulation
    function drawLines(p) {
        if (!p.active) return;
        for (var i in p.closest) {
            ctx.beginPath();
            ctx.moveTo(p.x, p.y);
            ctx.lineTo(p.closest[i].x, p.closest[i].y);
            ctx.strokeStyle = 'rgba(156,217,249,' + p.active + ')';
            ctx.stroke();
        }
    }

    function Circle(pos, rad, color) {
        var _this = this;
        // constructor
        (function () {
            _this.pos = pos || null;
            _this.radius = rad || null;
            _this.color = color || null;
        })();

        this.draw = function () {
            if (!_this.active) return;
            ctx.beginPath();
            ctx.arc(_this.pos.x, _this.pos.y, _this.radius, 0, 2 * Math.PI, false);
            ctx.fillStyle = 'rgba(156,217,249,' + _this.active + ')';
            ctx.fill();
        };
    }

    // Util
    function getDistance(p1, p2) {
        return Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2);
    }

})();

(function () {
    function login(container) {
        this.$container = container;
        this.$err = this.$container.querySelector(".error");
        this.$frm = this.$container.querySelector("form[name='login']");
        this.$logName = this.$container.querySelector("input[name='logname']");
        this.$logPass = this.$container.querySelector("input[name='logpass']");
        this.$logCode = this.$container.querySelector("input[name='logcode']");
        this.$logCodeImg = this.$container.querySelector(".code-img");
        this.$actionSubmit = this.$container.querySelector(".act-but");
        this.init();
    }
    login.prototype = {
        constructor: login,
        Ruler: {
            uName: /[a-zA-Z0-9]{6,11}/,
            uPass: /[\w\W]{6,20}/,
            uCode: /[a-zA-Z0-9]{4,8}/
        },
        init: function () {
            this.addListeners();
        },
        errMessage: function (msg) {
            this.$err.innerText = msg;
        },
        ImageChange: function (event) {
            try {
                event.currentTarget.src = "/Sys/ImageCode?codeType=sys_login_code&t=" + ~new Date();
            } catch (e) {
                this.errMessage(e.message);
            }
        },
        SubmitLogin: function (event, u) {
            var u = u || "/Sys/SysLogin";
            var that = this;
            that.setActionState(true);
            var isValid = that.inputChecker();
            if (isValid && JSON.stringify(isValid) != '{}') {
                var target = event.currentTarget || that.$actionSubmit;
                api.Ajax({
                    url: u,
                    data: isValid,
                    success: function (result) {
                        if (result.Error) {
                            that.errMessage(result.Msg);
                        } else {
                            var frm = api.QueryString("from");
                            if (frm) {
                                window.location.href = frm;
                            } else {
                                if (result.go) {
                                    window.location.href = result.go;
                                } else {
                                    window.location.href = "/Home/Index";
                                }
                            }
                        }
                    },
                    complete: function () {
                        that.setActionState(false);
                    }
                });
            } else {
                console.log("valid failed");
                that.setActionState(false);
            }
        },
        inputChecker: function () {
            var uName = this.$logName.value;
            console.log("uName:", uName);
            var uNameMatch = this.Ruler.uName.test(uName);
            var uPass = this.$logPass.value;
            console.log("uPass:", uPass);
            var uPassMatch = this.Ruler.uPass.test(uPass);
            var uCode = this.$logCode.value;
            console.log("uCode:", uCode);
            var uCodeMatch = this.Ruler.uCode.test(uCode);
            console.log("uNameMatch:", uNameMatch);
            console.log("uPassMatch:", uPassMatch);
            console.log("uCodeMatch:", uCodeMatch);
            var result = {};
            if (uNameMatch && uPassMatch && uCodeMatch) {
                result = {
                    phone: uName,
                    pwd: uPass,
                    code: uCode
                };
            } else {
                result = null;
            }
            return result;
        },
        setActionState: function (isDisable) {
            console.log("setActionState", isDisable);
            if (isDisable) {
                this.$actionSubmit.setAttribute("disabled", "disabled");
            } else {
                this.$actionSubmit.removeAttribute("disabled");
            }
        },
        addListeners: function () {
            this.$logCodeImg.removeEventListener("click", (event) => { this.ImageChange(event); }, true);
            this.$logCodeImg.addEventListener("click", (event) => { this.ImageChange(event); }, true);
            var u = "/Sys/SysLogin";
            this.$actionSubmit.removeEventListener("click", (event) => { this.SubmitLogin(event, u); }, true);
            this.$actionSubmit.addEventListener("click", (event) => { this.SubmitLogin(event, u); }, true);
        }
    }
    return new login(document.querySelector("#larger-header"));
})();