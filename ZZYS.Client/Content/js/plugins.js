﻿// 在缺少控制台的浏览器中避免“控制台”错误。
(function () {
    var method;
    var noop = function () { };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
(function () {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame']
            || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function (callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function () { callback(currTime + timeToCall); },
                timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function (id) {
            clearTimeout(id);
        };
}());

var globalConfig = {
    "IsDebug": true,
    "ApiDomain": ""
};

(function (win, cfg) {
    var api = {};
    api.GlobalConfig = cfg;

    api.Stringify = function (json) {
        if (typeof json === 'object') {
            return JSON && JSON.stringify(json);
        }
    };
    api.ParseJson = function (str) {
        if (typeof str === 'string') {
            return JSON && JSON.parse(str);
        }
    };
    api.Ajax = function (opt) {
        opt = opt || {};
        opt.method = (opt.method || 'POST').toUpperCase();
        opt.url = opt.url || '';
        opt.dataType = opt.dataType || "JSON";
        opt.async = opt.async || true;
        opt.data = opt.data || {};
        //opt.data['Key'] = window.localStorage.getItem(CONFIG.LOGIN_KEY);
        //opt.data['v'] = new Date().getTime();
        opt.success = opt.success || function () { };
        opt.error = function (xhr, sta) {
            if (typeof (opt.error) == 'function') {
                opt.error(xhr, sta);
            }
        };
        opt.complete = opt.complete || function () { console.log("default complete."); };

        if (!XMLHttpRequest) {
            console.log("浏览器不支持XMLHttpRequest对象.");
        }
        var xhr = new XMLHttpRequest();
        xhr.timeout = 50000;

        if (xhr == undefined || xhr == null) {
            console.log("XMLHttpRequest对象创建失败！！");
        }
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var json = JSON.parse((xhr.responseText || xhr.response) || '{"Error": true, "Msg":"请求数据失败，请稍后重试.plus"}');
                    if (typeof (json.IsLogin) != 'undefined' && !json.IsLogin) {
                        alert('请重新登录');

                    } else {
                        opt.success(json);
                    }
                } else {
                    opt.error(xhr, xhr.status);
                }
            }
        };

        var params = [];
        for (var key in opt.data) {
            if (opt.data.hasOwnProperty(key)) {
                params.push(key + '=' + opt.data[key]);
            };
        }
        try {
            var postData = params.join('&');
            if (opt.method.toUpperCase() === 'POST') {
                xhr.open(opt.method, opt.url, opt.async);
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
                xhr.send(postData);
            }
            else if (opt.method.toUpperCase() === 'GET') {
                xhr.open(opt.method, opt.url + '?' + postData, opt.async);
                xhr.send(null);
            }
        } catch (e) {
            console.log(e)
        }
        finally {
            if (opt.complete && typeof (opt.complete) != 'undefined' && typeof (opt.complete) == 'function') {
                opt.complete();
            }
        }
    };
    /*
     * 参数说明：
     * @param：要格式化的数字
     * @param：保留几位小数
     * @param：小数点符号
     * @param：千分位符号
     * */
    api.NumberFormat = function (number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.ceil(n * k) / k;
            };

        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        var re = /(-?\d+)(\d{3})/;
        while (re.test(s[0])) {
            s[0] = s[0].replace(re, "$1" + sep + "$2");
        }

        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }
    api.QueryString = function (name) {
        var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
        var r = window.location.search.substr(1).match(reg);

        if (r != null) {
            return decodeURIComponent(r[2]);
        }
        return null;
    }
    api.MainArticleWidth = 0;
    api.AsiderWidth = 0;
    api.init = function () {
        //主体内容宽度
        var article = document.querySelector(".mainContainer");
        if (article) {
            api.MainArticleWidth = article.clientWidth;
        }
        //导航菜单宽度
        var asider = document.querySelector(".aslider");
        if (asider) {
            api.AsiderWidth = asider.clientWidth;
        }
        //总宽度
        var totalWidth = document.querySelector(".layout");
        if (totalWidth) {
            api.TotalWidth = totalWidth.clientWidth;
        }
    }
    api.init();
    //@_@
    win.api = api;
})(window, globalConfig);
