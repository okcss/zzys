﻿using Common.Configure.Supports;
using System;

namespace Common.Configure.Interfaces
{
    internal interface IConfig
    {
        event EventHandler<ChangedEventArgs> PropertyChanged;
        void PropChanged(string propName, dynamic oldValue, dynamic newValue);
    }
}
