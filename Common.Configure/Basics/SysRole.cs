﻿using Common.Configure.Configs;
using Common.Configure.Supports;
using System;
using System.Collections.Generic;
using System.Linq;
using static Common.Configure.Supports.ConfigEnums;

namespace Common.Configure.Basics
{
    /// <summary>
    /// 角色表
    /// </summary>
    [Serializable]
    public class SysRole:IComparable
    {
        /// <summary>
        /// 角色编号
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public Guid Id { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public string RoleName { get; set; }
        /// <summary>
        /// 角色状态
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public AnyStatus State { get; set; }
        /// <summary>
        /// 状态名称
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Ignore)]
        public string StateText => State.GetDescription();
        /// <summary>
        /// 排序编号
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public decimal Sort { get; set; }
        public int CompareTo(object obj)
        {
            return Sort.CompareTo(obj);
        }

        #region 实体映射,循环引用
        /// <summary>
        /// 当前角色所对应的权限列表
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Ignore)]
        public virtual ICollection<SysPermissions> SysPermissions
        {
            get
            {
                try
                {
                    var permissionsConfig = ConfigSingle<PermissionsConfig>.Instance;
                    if (permissionsConfig?.Permissions == null)
                    {
                        ConfigLog.Error("SysRole.SysPermissions.Get Error:permissionsConfig.Permissions.数据为空!");
                        return new HashSet<SysPermissions>();
                    }
                    else
                    {
                        return permissionsConfig.Permissions.Where(e => e.RoleId == Id && e.State == AnyStatus.Valid).ToList();
                    }
                }
                catch (Exception e)
                {
                    ConfigLog.Error(string.Format("SysRole.SysPermissions.Get Error:{0}", e.InnerMessager()));
                    return new HashSet<SysPermissions>();
                }
            }
        }

        /// <summary>
        /// 当前角色所包含的管理员列表
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Ignore)]
        public virtual ICollection<SysManager> SysManager
        {
            get
            {
                try
                {
                    var adminsConfig = ConfigSingle<AdminsConfig>.Instance;
                    if (adminsConfig?.Administrators == null)
                    {
                        ConfigLog.Error("SysRole.SysManager.Get Error:adminsConfig.Administrators.数据为空!");
                        return new HashSet<SysManager>();
                    }
                    else
                    {
                        return adminsConfig.Administrators.Where(e => e.RoleId == Id && e.State == AnyStatus.Valid).ToList();
                    }
                }
                catch (Exception e)
                {
                    ConfigLog.Error(string.Format("SysRole.SysManager.Get Error:{0}", e.InnerMessager()));
                    return new HashSet<SysManager>();
                }
            }
        }
        #endregion
    }
}
