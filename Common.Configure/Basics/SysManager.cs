﻿using Common.Configure.Configs;
using Common.Configure.Supports;
using System;
using System.Linq;
using static Common.Configure.Supports.ConfigEnums;

namespace Common.Configure.Basics
{
    [Serializable]
    public class SysManager:IComparable
    {
        /// <summary>
        /// 管理员编号
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public Guid Id { get; set; }
        /// <summary>
        /// 管理员名称
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public string DisplayName { get; set; }
        /// <summary>
        /// 手机号(登陆账号)
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public string Phone { get; set; }
        /// <summary>
        /// 登陆密码
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public string Password { get; set; }
        /// <summary>
        /// 账号状态
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public AnyStatus State { get; set; }
        /// <summary>
        /// 账号加密key
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public Guid SysCode { get; set; }
        /// <summary>
        /// 角色编号
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public Guid RoleId { get; set; }
        /// <summary>
        /// 上次登陆IP
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public string LoginIP { get; set; }
        /// <summary>
        /// 上次登陆时间
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public DateTime LastLoginTime { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public decimal Sort { get; set; }
        /// <summary>
        /// 状态名称
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Ignore)]
        public string StateText => State.GetDescription();
        /// <summary>
        /// 实现排序接口
        /// </summary>
        /// <param name="obj">Sort</param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            return Sort.CompareTo(obj);
        }

        #region 实体映射,循环引用
        /// <summary>
        /// 获取当前管理员所属的角色对象
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Ignore)]
        public virtual SysRole SysRole
        {
            get
            {
                try
                {
                    var rolesConfig = ConfigSingle<RolesConfig>.Instance;
                    if (rolesConfig?.Roles == null)
                    {
                        ConfigLog.Error("SysManager.SysRole.Get Error:从roleConfig.Roles数据为空.");
                        return default(SysRole);
                    }
                    else
                    {
                        var sysRole = rolesConfig.Roles.FirstOrDefault(e => e.Id == RoleId && e.State == AnyStatus.Valid);
                        if (sysRole == null)
                        {
                            ConfigLog.Error(string.Format("SysManager.SysRole.Get Error:从roleConfig.Roles中未查询到RoleId={0}的角色数据.", RoleId));
                            return default(SysRole);
                        }
                        else
                        {
                            return sysRole;
                        }
                    }
                }
                catch (Exception e)
                {
                    ConfigLog.Error(string.Format("SysManager.SysRole.Get Error:{0}", e.InnerMessager()));
                    return default(SysRole);
                }
            }
        }
        #endregion
    }
}
