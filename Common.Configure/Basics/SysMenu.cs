﻿using Common.Configure.Abstracts;
using Common.Configure.Configs;
using Common.Configure.Supports;
using System;
using System.Collections.Generic;
using System.Linq;
using static Common.Configure.Supports.ConfigEnums;

namespace Common.Configure.Basics
{
    /// <inheritdoc />
    /// <summary>
    /// 系统菜单表
    /// </summary>
    [Serializable]
    public sealed class SysMenu : IComparable
    {
        public SysMenu()
        {
            ParentId = ConfigStatic.DefaultParentId;
        }
        /// <summary>
        /// 菜单编号
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public Guid Id { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public string DisplayName { get; set; }
        /// <summary>
        /// 链接地址
        /// </summary>
        [ConfigExtend(LocalType = LocalType.CData)]
        public string Href { get; set; }
        /// <summary>
        /// 链接图标
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public string ICON { get; set; }
        /// <summary>
        /// 是否显示在菜单列表
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public bool IsDisplay { get; set; }
        /// <summary>
        /// 所属上级节点
        /// <para>顶级节点默认值是:ConfigStatic.DefaultParentId is '88888888-8888-8888-8888-888888888888'</para>
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public Guid ParentId { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public decimal Sort { get; set; }
        /// <summary>
        /// 菜单索引(唯一)
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public string TabIndex { get; set; }
        /// <summary>
        /// 菜单状态
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public AnyStatus State { get; set; }
        /// <summary>
        /// 菜单等级(0,1,2,4)
        /// <para>0:未知,无效属性</para>
        /// <para>1:顶级菜单</para>
        /// <para>2:二级菜单</para>
        /// <para>4:功能按钮</para>
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public MenuLevel MenuLevel { get; set; }

        /// <summary>
        /// 状态名称
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Ignore)]
        public string StateText => State.GetDescription();
        /// <summary>
        /// 父节点名称
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Ignore)]
        public string ParentName
        {
            get
            {
                var result = string.Empty;
                if (ParentId == ConfigStatic.DefaultParentId)
                {
                    return "顶级节点";
                }
                var allMenus = ConfigSingle<MenusConfig>.Instance;
                if (allMenus?.Menus == null || allMenus.Menus.Count == 0)
                {
                    return result;
                }
                var pMenu = allMenus.Menus.FirstOrDefault(e => e.Id == ParentId);
                if (pMenu != null)
                {
                    result = pMenu.DisplayName;
                }
                return result;
            }
        }
        #region 实体映射,循环引用
        /// <summary>
        /// 该权限菜单对应的权限列表
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Ignore)]
        public ICollection<SysPermissions> SysPermissions
        {
            get
            {
                try
                {
                    var permissionsConfig = ConfigSingle<PermissionsConfig>.Instance;
                    if (permissionsConfig?.Permissions == null)
                    {
                        ConfigLog.Error("SysMenu.SysPermissions.Get Error:permissionsConfig.Permissions.");
                        return new HashSet<SysPermissions>();
                    }
                    else
                    {
                        return permissionsConfig.Permissions.Where(e => e.MenuId == Id && e.State == AnyStatus.Valid).ToList();
                    }
                }
                catch (Exception e)
                {
                    ConfigLog.Error(string.Format("SysMenu.SysPermissions.Get Error:{0}", e.InnerMessager()));
                    return new HashSet<SysPermissions>();
                }
            }
        }

        public int CompareTo(object obj)
        {
            return Sort.CompareTo(obj);
        }
        #endregion
    }
}
