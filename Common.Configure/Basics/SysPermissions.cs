﻿using Common.Configure.Abstracts;
using Common.Configure.Configs;
using Common.Configure.Supports;
using System;
using System.Linq;
using static Common.Configure.Supports.ConfigEnums;

namespace Common.Configure.Basics
{
    /// <summary>
    /// 角色权限映射表
    /// </summary>
    [Serializable]
    public class SysPermissions:IComparable
    {
        /// <summary>
        /// ID
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public Guid Id { get; set; }
        /// <summary>
        /// 角色ID
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public Guid RoleId { get; set; }
        /// <summary>
        /// 菜单ID
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public Guid MenuId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 有效状态
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public AnyStatus State { get; set; }
        /// <summary>
        /// 状态名称
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Ignore)]
        public string StateText => State.GetDescription();
        /// <summary>
        /// 排序编号
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Attribute)]
        public decimal Sort { get; set; }
        public int CompareTo(object obj)
        {
            return Sort.CompareTo(obj);
        }

        #region 实体映射,循环引用
        /// <summary>
        /// 当前映射对应的 菜单对象
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Ignore)]
        public virtual SysMenu SysMenu
        {
            get
            {
                try
                {
                    var menusConfig = ConfigSingle<MenusConfig>.Instance;
                    if (menusConfig?.Menus == null)
                    {
                        ConfigLog.Error("SysPermissions.SysMenu.Get Error:menusConfig.Menus.数据为空!");
                        return default(SysMenu);
                    }
                    else
                    {
                        var sysMenu = menusConfig.Menus.FirstOrDefault(e => e.Id == MenuId && e.State == AnyStatus.Valid);
                        if (sysMenu == null)
                        {
                            ConfigLog.Error(string.Format("SysPermissions.SysMenu.Get Error:menusConfig.Menus中未查询到MenuId={0}的角色数据.", MenuId));
                            return default(SysMenu);
                        }
                        else
                        {
                            return sysMenu;
                        }
                    }
                }
                catch (Exception e)
                {
                    ConfigLog.Error(string.Format("SysPermissions.SysMenu.Get Error:{0}", e.InnerMessager()));
                    return default(SysMenu);
                }
            }
        }
        /// <summary>
        /// 当前映射对应的 角色对象
        /// </summary>
        [ConfigExtend(LocalType = LocalType.Ignore)]
        public virtual SysRole SysRole
        {
            get
            {
                try
                {
                    var rolesConfig = ConfigSingle<RolesConfig>.Instance;
                    if (rolesConfig?.Roles == null)
                    {
                        ConfigLog.Error("SysPermissions.SysRole.Get Error:rolesConfig.Roles.数据为空!");
                        return default(SysRole);
                    }
                    else
                    {
                        var sysRole = rolesConfig.Roles.FirstOrDefault(e => e.Id == RoleId && e.State == AnyStatus.Valid);
                        if (sysRole == null)
                        {
                            ConfigLog.Error(string.Format("SysPermissions.SysRole.Get Error:从rolesConfig.Roles中未查询到RoleId={0}的角色数据.", RoleId));
                            return default(SysRole);
                        }
                        else
                        {
                            return sysRole;
                        }
                    }
                }
                catch (Exception e)
                {
                    ConfigLog.Error(string.Format("SysPermissions.SysRole.Get Error:{0}", e.InnerMessager()));
                    return default(SysRole);
                }
            }
        }
        #endregion
    }
}
