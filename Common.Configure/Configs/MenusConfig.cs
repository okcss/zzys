﻿using Common.Configure.Abstracts;
using Common.Configure.Basics;
using Common.Configure.Supports;
using System;
using System.Collections.Generic;

namespace Common.Configure.Configs
{
    /// <summary>
    /// 权限菜单配置
    /// </summary>
    [Serializable]
    public class MenusConfig : BaseConfig
    {
        public MenusConfig()
        {
            _Menus = new List<SysMenu>();
        }

        private readonly List<SysMenu> _Menus;
        /// <summary>
        /// 权限菜单列表
        /// </summary>
        [ConfigExtend(TargetType = typeof(SysMenu),
                      LocalType = ConfigEnums.LocalType.Array,
                      TargetNode = "Menu")]
        public List<SysMenu> Menus => _Menus;

    }
}
