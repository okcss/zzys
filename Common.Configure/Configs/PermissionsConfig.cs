﻿using Common.Configure.Abstracts;
using Common.Configure.Basics;
using Common.Configure.Supports;
using System;
using System.Collections.Generic;
using static Common.Configure.Supports.ConfigEnums;

namespace Common.Configure.Configs
{
    /// <summary>
    /// 角色权限配置
    /// </summary>
    [Serializable]
    public class PermissionsConfig : BaseConfig
    {
        public PermissionsConfig()
        {
            _Permissions = new List<SysPermissions>();
        }

        private List<SysPermissions> _Permissions;
        /// <summary>
        /// 所有角色对应权限列表
        /// </summary>
        [ConfigExtend(TargetType = typeof(SysPermissions),
                      LocalType = LocalType.Array,
                      TargetNode = "Permission")]
        public List<SysPermissions> Permissions => _Permissions;

    }
}
