﻿using Common.Configure.Abstracts;
using Common.Configure.Basics;
using Common.Configure.Supports;
using System;
using System.Collections.Generic;

namespace Common.Configure.Configs
{
    /// <summary>
    /// 角色配置
    /// </summary>
    [Serializable]
    public class RolesConfig : BaseConfig
    {
        public RolesConfig()
        {
            _Roles = new List<SysRole>();
        }

        private readonly List<SysRole> _Roles;
        /// <summary>
        /// 角色列表
        /// </summary>
        [ConfigExtend(TargetType = typeof(SysRole),
                      LocalType = ConfigEnums.LocalType.Array,
                      TargetNode = "Role")]
        public List<SysRole> Roles => _Roles;
    }
}
