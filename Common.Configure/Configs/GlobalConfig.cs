﻿using Common.Configure.Abstracts;

namespace Common.Configure.Configs
{
    public class GlobalConfig : BaseConfig
    {
        public string Logo { get; set; }
        public string AppName { get; set; }
        public string CompanyName { get; set; }
        public string ICPCode { get; set; }
        public string QRCode { get; set; }
        public string ServiceQQ { get; set; }
        public string AdminPhone { get; set; }
        public string Copyright { get; set; }

        public int DefaultPageSize { get; set; }
        public int AbsoluteInterval { get; set; }
        public int SlidingInterval { get; set; }
    }
}
