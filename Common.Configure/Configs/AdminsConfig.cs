﻿using Common.Configure.Abstracts;
using Common.Configure.Basics;
using Common.Configure.Supports;
using System;
using System.Collections.Generic;

namespace Common.Configure.Configs
{
    /// <summary>
    /// 管理员配置
    /// </summary>
    [Serializable]
    public class AdminsConfig : BaseConfig
    {
        public AdminsConfig()
        {
            _Administrators = new List<SysManager>();
        }
        private readonly List<SysManager> _Administrators;
        /// <summary>
        /// 管理员列表
        /// </summary>
        [ConfigExtend(TargetType = typeof(SysManager),
                      LocalType = ConfigEnums.LocalType.Array,
                      TargetNode = "Admin")]
        public List<SysManager> Administrators => _Administrators;
    }
}
