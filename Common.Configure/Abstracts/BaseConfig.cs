﻿using Common.Configure.Interfaces;
using Common.Configure.Supports;
using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Xml.Linq;
using static Common.Configure.Supports.ConfigEnums;

namespace Common.Configure.Abstracts
{
    public abstract class BaseConfig : IConfig
    {
        #region internal
        /// <summary>
        /// 获取配置文件是否已加载成功。
        /// </summary>
        internal bool IsLoaded { get; set; }
        #endregion

        #region Common Properties 配置文件通用属性        
        protected Guid _ConfigId;
        protected DateTime _ModifyDate;
        /// <summary>
        /// 配置文件唯一标示
        /// </summary>
        [ConfigExtend(LocalType = LocalType.RootProperty)]
        public Guid ConfigId
        {
            get
            {
                return _ConfigId == Guid.Empty ? Guid.NewGuid() : _ConfigId;
            }
            protected set
            {
                PropChanged("ConfigId", _ConfigId, value);
                _ConfigId = value;
            }
        }
        /// <summary>
        /// 配置文件的最后修改时间
        /// </summary>
        [ConfigExtend(LocalType = LocalType.RootProperty)]
        public DateTime ModifyDate
        {
            get { return _ModifyDate; }
            protected set
            {
                PropChanged("ModifyDate", _ModifyDate, value);
                _ModifyDate = value;
            }
        }
        #endregion

        #region Private Properties 私有属性
        /// <summary>
        /// 配置文件的完整路径
        /// </summary>
        private string ConfigFullName => $"{ConfigStatic.ConfigRoot}\\{GetType().Name}{ConfigStatic.ConfigExt}";
        /// <summary>
        /// 获取或设置配置文件的 XDocument 对象。
        /// </summary>
        private XDocument ConfigDocument { get; set; }
        #endregion

        #region Virtual Methods 虚函数
        #region Load 加载配置文件
        /// <summary>
        /// 加载配置文件
        /// </summary>
        public void LoadConfig()
        {
            #region 数据校验
            if (string.IsNullOrWhiteSpace(ConfigFullName) || !File.Exists(ConfigFullName))
            {
                IsLoaded = false;
                return;
            }
            ConfigDocument = XDocument.Load(ConfigFullName);
            if (ConfigDocument.Root == null)
            {
                IsLoaded = false;
                return;
            }
            #endregion

            var fields = GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var field in fields)
            {
                #region 获取当前属性在xml文档中的配置信息，如果未配置，则创建为默认值的扩展属性
                var propExtend = field.GetCustomAttribute<ConfigExtend>()
                                 ?? new ConfigExtend
                                 {
                                     LocalType = LocalType.Default,
                                     TargetType = field.PropertyType,
                                     TargetNode = string.Empty
                                 };
                #endregion
                #region 忽略的属性
                if (propExtend.LocalType == LocalType.Ignore)
                {
                    continue;
                }
                #endregion
                var elName = field.Name.TrimStart('_');
                #region 读取根节点属性
                if (propExtend.LocalType == LocalType.RootProperty)
                {
                    var attr = ConfigDocument.Root.Attribute(elName);
                    ConfigStatic.SetTargetValue(this, attr?.Value ?? string.Empty, field);
                }
                #endregion
                else
                {
                    var xElement = ConfigDocument.Root.Element(elName);
                    if (xElement == null) continue;
                    if (propExtend.LocalType == LocalType.Array && !string.IsNullOrWhiteSpace(propExtend.TargetNode))
                    {
                        var els = xElement.Elements(propExtend.TargetNode);
                        var arrayFieldObject = field.GetValue(this);
                        if (arrayFieldObject == null)
                        {
                            #region 
                            //var generic = typeof(List<>);
                            //var genericType = new[] { propExtend.ArrayType };
                            //generic = generic.MakeGenericType(genericType);
                            //
                            //arrayFieldObject = Activator.CreateInstance(generic) as IList;
                            //
                            #endregion
                            continue;
                        }
                        var arrayFieldAdd = field.PropertyType.GetMethod("Add");
                        if (arrayFieldAdd == null) { continue; }
                        ConfigStatic.LoadArray(arrayFieldObject, field, propExtend, els);
                    }
                    else if (propExtend.LocalType == LocalType.Object && !string.IsNullOrWhiteSpace(propExtend.TargetNode))
                    {
                        //var el = xElement.Element(propExtend.TargetNode);
                        //if (el == null) { el = new XElement(propExtend.TargetNode); }
                        var obj = field.GetValue(this);
                        if (obj == null) { continue; }
                        ConfigStatic.LoadObject(xElement, obj, propExtend);
                    }
                    else
                    {
                        string v;
                        if (propExtend.LocalType != LocalType.Attribute)
                        {
                            v = xElement.Value;
                        }
                        else
                        {
                            var attr = xElement.Attribute(elName);
                            v = attr?.Value ?? string.Empty;
                        }
                        ConfigStatic.SetTargetValue(this, v, field);
                    }
                }
            }
            IsLoaded = true;
        }
        #endregion
        #region Save 保存配置文件
        /// <summary>
        /// 保存配置文件
        /// </summary>
        public void SaveConfig()
        {
            if (!IsLoaded) return;
            if (ConfigDocument == null) throw new Exception("配置文件还未加载");
            if (ConfigDocument.Root == null) throw new Exception("配置文件还未加载");
            ModifyDate = DateTime.Now;
            var fields = GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var field in fields)
            {
                #region 获取当前属性在xml文档中的配置信息，如果未配置，则创建为默认值的扩展属性 (第一层，propExtend)
                var propExtend = field.GetCustomAttribute<ConfigExtend>()
                                 ?? new ConfigExtend
                                 {
                                     LocalType = LocalType.Default,
                                     TargetType = field.PropertyType,
                                     TargetNode = string.Empty
                                 };
                #endregion
                if (propExtend.LocalType == LocalType.Ignore) { continue; }
                if (field.GetMethod.IsVirtual) { continue; }
                #region 获取实例对象中当前属性的（值，节点名称，XDoc节点对象）
                var elValue = field.GetValue(this)?.ToString() ?? string.Empty;
                var elName = field.Name.TrimStart('_');

                var xElement = ConfigDocument.Root.Element(elName);
                #endregion
                #region 是根节点属性
                if (propExtend.LocalType == LocalType.RootProperty)
                {
                    ConfigDocument.Root.SetAttributeValue(elName, elValue);
                }
                #endregion
                else
                {
                    #region 节点不存在，创建节点并加入到 ConfigDocument 中
                    if (xElement == null)
                    {
                        xElement = new XElement(elName);
                        ConfigDocument.Root.Add(xElement);
                    }
                    #endregion

                    if (propExtend.LocalType == LocalType.Array && !string.IsNullOrWhiteSpace(propExtend.TargetNode))
                    {
                        var arrayModels = field.GetValue(this) as IEnumerable;
                        if (arrayModels == null) continue;
                        ConfigStatic.SaveArray(xElement, arrayModels, field);
                    }
                    else if (propExtend.LocalType == LocalType.Object && !string.IsNullOrWhiteSpace(propExtend.TargetNode))
                    {
                        var obj = field.GetValue(this);
                        if (obj == null) continue;
                        ConfigStatic.SaveObject(xElement, obj, propExtend);
                    }
                    #region Attribute数据保存
                    //保存xml的属性节点（attribute）
                    else if (propExtend.LocalType == LocalType.Attribute)
                    {
                        xElement.SetAttributeValue(elName, elValue);
                    }
                    #endregion
                    #region CData数据保存
                    //CData数据保存
                    else if (propExtend.LocalType == LocalType.CData)
                    {
                        var cData = new XCData(elValue);
                        var cDataNode = new XElement(xElement.Name, cData);
                        cDataNode.SetAttributeValue("IsCData", bool.TrueString);
                        xElement.ReplaceWith(cDataNode);
                    }
                    #endregion
                    #region 普通数据保存
                    //普通数据保存
                    else
                    {
                        var node = xElement.Name == elName ? xElement : null;
                        if (node == null)
                        {
                            var cNode = new XElement(elName) { Value = elValue };
                            xElement.Add(cNode);
                        }
                        else
                        {
                            node.Value = elValue;
                        }
                    }
                    #endregion
                }
            }
            ConfigDocument.Save(ConfigFullName);
        }
        #endregion
        #endregion

        public event EventHandler<ChangedEventArgs> PropertyChanged;
        public void PropChanged(string propName, dynamic oldValue, dynamic newValue)
        {
            var args = new ChangedEventArgs(GetType().Name, propName, oldValue, newValue);
            PropertyChanged?.Invoke(this, args);
        }

    }
}
