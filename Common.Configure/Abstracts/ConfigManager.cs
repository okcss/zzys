﻿using Common.Configure.Supports;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Common.Configure.Abstracts
{
    internal static class ConfigManager
    {
        internal static event EventHandler<ChangedEventArgs> PropertyChanged;
        private static readonly Dictionary<Type, BaseConfig> _Cache = new Dictionary<Type, BaseConfig>();

        static ConfigManager()
        {
            ReloadAllConfig();
        }
        /// <summary>
        /// 获取配置文件对象。
        /// </summary>
        /// <typeparam name="T">配置文件类型</typeparam>
        /// <returns></returns>
        internal static T GetConfig<T>() where T : BaseConfig
        {
            var t = typeof(T);
            BaseConfig config;
            if (!_Cache.TryGetValue(t, out config)) return default(T);
            if (!config.IsLoaded) return default(T);
            return (T)config;
        }
        /// <summary>
        /// 重新加载所有配置文件。
        /// </summary>
        public static void ReloadAllConfig()
        {
            _Cache.Clear();

            var assembly = Assembly.GetExecutingAssembly();
            var baseConfigType = typeof(BaseConfig);

            foreach (var configClass in assembly.ExportedTypes)
            {
                if (configClass.BaseType != baseConfigType) continue;
                if (string.IsNullOrWhiteSpace(configClass.FullName)) continue;
                var config = (BaseConfig)assembly.CreateInstance(configClass.FullName);
                if (config == null) { return; }
                config.LoadConfig();
                config.PropertyChanged += config_PropertyChanged;
                Add(config);
            }
        }
        internal static void config_PropertyChanged(object sender, ChangedEventArgs e)
        {
            PropertyChanged?.Invoke(sender, e);
        }
        private static void Add(BaseConfig config)
        {
            _Cache.Add(config.GetType(), config);
        }

        /// <summary>
        /// 重新加载指定的配置文件。
        /// </summary>
        /// <typeparam name="T">配置文件类型</typeparam>
        public static void ReloadConfig<T>()
        {
            var t = typeof(T);
            BaseConfig config;
            if (_Cache.TryGetValue(t, out config))
            {
                config.LoadConfig();
            }
        }

        /// <summary>
        /// 保存所有已修改的配置文件。
        /// </summary>
        public static void SaveAllConfig()
        {
            foreach (var key in _Cache.Keys)
            {
                _Cache[key].SaveConfig();
            }
        }

        /// <summary>
        /// 保存加载指定的配置文件。
        /// </summary>
        /// <typeparam name="T">配置文件类型</typeparam>
        public static void SaveConfig<T>()
        {
            var t = typeof(T);
            BaseConfig config;
            if (_Cache.TryGetValue(t, out config))
            {
                config.SaveConfig();
            }
        }
    }
}
