﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Common.Configure.Supports
{
    /// <summary>
    /// 配置文件枚举
    /// </summary>
    public static class ConfigEnums
    {
        #region AnyStatus:内容的属性状态
        [Flags]
        public enum AnyStatus
        {
            /// <summary>
            /// 有效状态
            /// </summary>
            [Description("有效")]
            Valid = 1,
            /// <summary>
            /// 无效状态
            /// </summary>
            [Description("无效")]
            Invalid = 2
        }
        #endregion

        #region LocalType:xml属性或节点的类型
        /// <summary>
        /// xml属性或节点的类型
        /// </summary>
        [Flags]
        internal enum LocalType
        {
            /// <summary>
            /// 默认（普通文本内容）
            /// </summary>
            Default = 1,
            /// <summary>
            /// CData内容
            /// </summary>
            CData = 2,
            /// <summary>
            /// 节点属性
            /// </summary>
            Attribute = 4,
            /// <summary>
            /// 泛型集合
            /// </summary>
            Array = 8,
            /// <summary>
            /// 根节点属性
            /// </summary>
            RootProperty = 16,
            /// <summary>
            /// 忽略该属性的设置
            /// </summary>
            Ignore = 32,
            /// <summary>
            /// 对象
            /// </summary>
            Object = 64
        }
        #endregion

        #region MenuLevel:SysMenus菜单级别
        /// <summary>
        /// SysMenus菜单级别
        /// </summary>
        [Flags]
        public enum MenuLevel
        {
            /// <summary>
            /// 未知类型
            /// </summary>
            [Description("未知类型")]
            None = 0,
            /// <summary>
            /// 顶级菜单
            /// </summary>
            [Description("顶级菜单")]
            Top = 1,
            /// <summary>
            /// 二级菜单
            /// </summary>
            [Description("二级菜单")]
            Menu = 2,
            /// <summary>
            /// 功能按钮
            /// </summary>
            [Description("功能按钮")]
            Func = 4
        }
        #endregion

        #region 公共方法(枚举描述,枚举转字典)
        /// <summary>
        /// 扩展方法，获得枚举的Description
        /// </summary>
        /// <param name="value">枚举值</param>
        /// <param name="nameInstead">当枚举值没有定义DescriptionAttribute，是否使用枚举名代替，默认是使用</param>
        /// <returns>枚举的Description</returns>
        public static string GetDescription(this Enum value, bool nameInstead = true)
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            if (name == null)
            {
                return string.Empty;
            }

            var field = type.GetField(name);
            var attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;

            if (attribute == null && nameInstead)
            {
                return name;
            }
            return attribute == null ? string.Empty : attribute.Description;
        }
        /// <summary>
        /// 把枚举转换为键值对集合
        /// </summary>
        /// <param name="enumType">枚举类型</param>
        /// <param name="getText">获得值得文本</param>
        /// <returns>以枚举值为key，枚举文本为value的键值对集合</returns>
        public static Dictionary<int, string> EnumToDictionary(Type enumType, Func<Enum, string> getText)
        {
            if (!enumType.IsEnum)
            {
                throw new ArgumentException("传入的参数必须是枚举类型！", "enumType");
            }
            var enumDic = new Dictionary<int, string>();
            var enumValues = Enum.GetValues(enumType);
            foreach (Enum enumValue in enumValues)
            {
                //enumValue.ToString();
                var key = Convert.ToInt32(enumValue);
                var value = getText(enumValue);
                enumDic.Add(key, value);
            }
            return enumDic;
        }
        #endregion
    }
}
