﻿using Common.Configure.Abstracts;

namespace Common.Configure.Supports
{
    /// <summary>
    /// 配置文件的泛型单例
    /// <para>使用方法:ConfigSingle&lt;GlobalConfig&gt;.Instance 即代表 GlobalConfig 配置对象的实例.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class ConfigSingle<T> where T : BaseConfig
    {
        private static T _Instance;
        private static readonly object syncLock = new object();
        /// <summary>
        /// <typeparam name="T">对象的实例</typeparam>
        /// </summary>
        public static T Instance
        {
            get
            {
                if (_Instance == null)
                {
                    lock (syncLock)
                    {
                        if (_Instance == null)
                        {
                            _Instance = ConfigManager.GetConfig<T>();
                        }
                    }
                }
                return _Instance;
            }
        }
    }
}
