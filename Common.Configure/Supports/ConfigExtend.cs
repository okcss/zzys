﻿using System;
using static Common.Configure.Supports.ConfigEnums;

namespace Common.Configure.Supports
{
    [AttributeUsage(AttributeTargets.Property)]
    internal class ConfigExtend : Attribute
    {
        /// <summary>
        /// 节点类型枚举
        /// </summary>
        public LocalType LocalType { get; set; } = LocalType.Default;
        /// <summary>
        /// 集合的泛型类型
        /// </summary>
        public Type TargetType { get; set; } = null;
        /// <summary>
        /// 集合的每个子节点名称
        /// </summary>
        public string TargetNode { get; set; } = string.Empty;
    }
}
