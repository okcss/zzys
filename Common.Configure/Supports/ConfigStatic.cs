﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web;
using System.Xml.Linq;
using static Common.Configure.Supports.ConfigEnums;

namespace Common.Configure.Supports
{
    /// <summary>
    /// Const Property 常量
    /// </summary>
    internal static partial class ConfigStatic
    {
        /// <summary>
        /// 配置文件扩展名
        /// </summary>
        public const string ConfigExt = ".xml";
        /// <summary>
        /// 配置文件相对路径
        /// </summary>
        private const string ConfigVirtual = "bin\\Configs";
        /// <summary>
        /// 配置文件日志信息
        /// </summary>
        private const string LogVirtual = "bin\\logs";


    }
    /// <summary>
    /// Static Property 静态属性
    /// </summary>
    internal static partial class ConfigStatic
    {
        #region 获取当前.dll在磁盘中的物理路径
        private static string _PhysicalPath;
        /// <summary>
        /// 获取当前.dll在磁盘中的物理路径
        /// </summary>
        public static string Physical
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(_PhysicalPath)) { return _PhysicalPath; }
                _PhysicalPath = AppDomain.CurrentDomain.BaseDirectory;
                return _PhysicalPath;
            }
        }
        #endregion

        #region 配置文件的完整目录路径
        private static string _ConfigRoot;
        /// <summary>
        /// 配置文件的完整目录路径
        /// </summary>
        public static string ConfigRoot
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(_ConfigRoot)) return _ConfigRoot;
                _ConfigRoot = Path.Combine(Physical, ConfigVirtual);
                return _ConfigRoot;
            }
        }
        #endregion

        #region 日志文件的目录路径

        public static string _LogRoot;
        /// <summary>
        /// 日志文件的目录路径
        /// </summary>
        public static string LogRoot
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(_LogRoot)) return _LogRoot;
                _LogRoot = Path.Combine(Physical, LogVirtual);
                if (!Directory.Exists(_LogRoot))
                {
                    Directory.CreateDirectory(_LogRoot);
                }
                return _LogRoot;
            }
        }

        #endregion

        /// <summary>
        /// 默认的非空固定Guid,用于默认的ParentId
        /// </summary>
        public static readonly Guid DefaultParentId = Guid.Parse("88888888-8888-8888-8888-888888888888");
    }
    /// <summary>
    /// Static Metohds 静态函数
    /// </summary>
    internal static partial class ConfigStatic
    {
        #region 将字段的值设置到对象上
        /// <summary>
        /// 将字段的值设置到对象上
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="val"></param>
        /// <param name="field"></param>
        internal static void SetTargetValue(object obj, string val, FieldInfo field)
        {
            if (field.FieldType == typeof(int))
            {
                int tmp;
                field.SetValue(obj, int.TryParse(val, out tmp) ? tmp : 0);
            }
            else if (field.FieldType == typeof(long))
            {
                long tmp;
                field.SetValue(obj, long.TryParse(val, out tmp) ? tmp : 0L);
            }
            else if (field.FieldType == typeof(decimal))
            {
                decimal tmp;
                field.SetValue(obj, decimal.TryParse(val, out tmp) ? tmp : 0m);
            }
            else if (field.FieldType == typeof(float))
            {
                float tmp;
                field.SetValue(obj, float.TryParse(val, out tmp) ? tmp : 0f);
            }
            else if (field.FieldType == typeof(double))
            {
                double tmp;
                field.SetValue(obj, double.TryParse(val, out tmp) ? tmp : 0d);
            }
            else if (field.FieldType == typeof(byte))
            {
                byte tmp;
                field.SetValue(obj, byte.TryParse(val, out tmp) ? tmp : 0);
            }
            else if (field.FieldType == typeof(DateTime))
            {
                DateTime tmp;
                field.SetValue(obj, DateTime.TryParse(val, out tmp) ? tmp : DateTime.Parse("1970/1/1"));
            }
            else if (field.FieldType == typeof(bool))
            {
                bool tmp;
                switch (val)
                {
                    case "0":
                        val = bool.FalseString;
                        break;
                    case "1":
                        val = bool.TrueString;
                        break;
                }
                field.SetValue(obj, bool.TryParse(val, out tmp) && tmp);
            }
            else if (field.FieldType == typeof(Guid))
            {
                Guid tmp;
                if (Guid.TryParse(val, out tmp) && tmp != Guid.Empty)
                {
                    field.SetValue(obj, tmp);
                }
                else
                {
                    field.SetValue(obj, Guid.NewGuid());
                }
            }
            else if (field.FieldType.BaseType == typeof(Enum))
            {
                field.SetValue(obj, Enum.Parse(field.FieldType, val));
            }
            else
            {
                field.SetValue(obj, val);
            }
        }
        internal static void SetTargetValue(object obj, string val, PropertyInfo field)
        {
            if (!field.CanWrite)
            {
                return;
            }
            if (field.PropertyType == typeof(int))
            {
                int tmp;
                field.SetValue(obj, int.TryParse(val, out tmp) ? tmp : 0);
            }
            else if (field.PropertyType == typeof(long))
            {
                long tmp;
                field.SetValue(obj, long.TryParse(val, out tmp) ? tmp : 0L);
            }
            else if (field.PropertyType == typeof(decimal))
            {
                decimal tmp;
                field.SetValue(obj, decimal.TryParse(val, out tmp) ? tmp : 0m);
            }
            else if (field.PropertyType == typeof(float))
            {
                float tmp;
                field.SetValue(obj, float.TryParse(val, out tmp) ? tmp : 0f);
            }
            else if (field.PropertyType == typeof(double))
            {
                double tmp;
                field.SetValue(obj, double.TryParse(val, out tmp) ? tmp : 0d);
            }
            else if (field.PropertyType == typeof(byte))
            {
                byte tmp;
                field.SetValue(obj, byte.TryParse(val, out tmp) ? tmp : 0);
            }
            else if (field.PropertyType == typeof(DateTime))
            {
                DateTime tmp;
                field.SetValue(obj, DateTime.TryParse(val, out tmp) ? tmp : DateTime.Parse("1970/1/1"));
            }
            else if (field.PropertyType == typeof(bool))
            {
                bool tmp;
                switch (val)
                {
                    case "0":
                        val = bool.FalseString;
                        break;
                    case "1":
                        val = bool.TrueString;
                        break;
                }
                field.SetValue(obj, bool.TryParse(val, out tmp) && tmp);
            }
            else if (field.PropertyType == typeof(Guid))
            {
                Guid tmp;
                if (Guid.TryParse(val, out tmp) && tmp != Guid.Empty)
                {
                    field.SetValue(obj, tmp);
                }
                else
                {
                    field.SetValue(obj, Guid.NewGuid());
                }
            }
            else if (field.PropertyType.BaseType == typeof(Enum))
            {
                field.SetValue(obj, Enum.Parse(field.PropertyType, val));
            }
            else
            {
                field.SetValue(obj, val);
            }
        }
        #endregion

        internal static void LoadArray(object array, FieldInfo arrayField, ConfigExtend propExtend, IEnumerable<XElement> elements)
        {
            #region 数据校验
            if (array == null) { ConfigLog.Error("读取配置文件中的集合类型,但config对象为空."); return; }
            if (elements == null) { ConfigLog.Error("读取配置文件中的集合类型,节点集合elements对象为空."); return; }
            if (propExtend == null) { ConfigLog.Error("从类型中读取该集合的ConfigExtend自定义属性失败;"); return; }
            var arrayAddMethod = arrayField.FieldType.GetMethod("Add");
            if (arrayAddMethod == null) { ConfigLog.Error("获取该类型的(Add)方法失败;"); return; }
            #endregion
            var fields = propExtend.TargetType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var ele in elements)
            {
                var target = Activator.CreateInstance(propExtend.TargetType);
                foreach (var field in fields)
                {
                    #region 泛型类型的每个属性的扩展信息 (第二层)
                    var prop2Extend = field.GetCustomAttribute<ConfigExtend>() ??
                                      new ConfigExtend
                                      {
                                          LocalType = LocalType.Attribute,
                                          TargetType = field.PropertyType,
                                          TargetNode = string.Empty
                                      };
                    #endregion
                    if (prop2Extend.LocalType == LocalType.Ignore) { continue; }
                    var elName = field.Name.TrimStart('_');
                    if (prop2Extend.LocalType == LocalType.Array &&
                        !string.IsNullOrWhiteSpace(prop2Extend.TargetNode))
                    {
                        var root = ele.Element(elName);
                        if (root == null) { continue; }
                        var els = root.Elements(prop2Extend.TargetNode);
                        var arrayFieldAdd = field.PropertyType.GetMethod("Add");
                        if (arrayFieldAdd == null) { continue; }
                        var arrayFieldObject = field.GetValue(target);
                        if (arrayFieldObject == null) { continue; }

                        LoadArray(arrayFieldObject, field, prop2Extend, els);
                    }
                    else
                    {
                        string v;
                        if (prop2Extend.LocalType == LocalType.Attribute)
                        {
                            var xAttr = ele.Attribute(elName);
                            v = xAttr?.Value;
                        }
                        else
                        {
                            var node = ele.Element(elName);
                            v = node?.Value;
                        }
                        SetTargetValue(target, v, field);
                    }
                }
                arrayAddMethod.Invoke(array, new[] { target });
            }
        }
        internal static void LoadArray(object array, PropertyInfo arrayField, ConfigExtend propExtend, IEnumerable<XElement> elements)
        {
            #region 数据校验
            if (array == null) { ConfigLog.Error("读取配置文件中的集合类型,但config对象为空."); return; }
            if (elements == null) { ConfigLog.Error("读取配置文件中的集合类型,节点集合elements对象为空."); return; }
            if (propExtend == null) { ConfigLog.Error("从类型中读取该集合的ConfigExtend自定义属性失败;"); return; }
            var arrayAddMethod = arrayField.PropertyType.GetMethod("Add");
            if (arrayAddMethod == null) { ConfigLog.Error("获取该类型的(Add)方法失败;"); return; }
            #endregion
            var fields = propExtend.TargetType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var ele in elements)
            {
                var target = Activator.CreateInstance(propExtend.TargetType);
                foreach (var field in fields)
                {
                    #region 泛型类型的每个属性的扩展信息 (第二层)
                    var prop2Extend = field.GetCustomAttribute<ConfigExtend>() ??
                                      new ConfigExtend
                                      {
                                          LocalType = LocalType.Attribute,
                                          TargetType = field.PropertyType,
                                          TargetNode = string.Empty
                                      };
                    #endregion
                    if (prop2Extend.LocalType == LocalType.Ignore) { continue; }
                    var elName = field.Name.TrimStart('_');

                    if (prop2Extend.LocalType == LocalType.Array &&
                        !string.IsNullOrWhiteSpace(prop2Extend.TargetNode))
                    {
                        var root = ele.Element(elName);
                        if (root == null) { continue; }
                        var els = root.Elements(prop2Extend.TargetNode);
                        var arrayFieldAdd = field.PropertyType.GetMethod("Add");
                        if (arrayFieldAdd == null) { continue; }
                        var arrayFieldObject = field.GetValue(target);
                        if (arrayFieldObject == null) { continue; }

                        LoadArray(arrayFieldObject, field, prop2Extend, els);
                    }
                    else
                    {
                        string v;
                        if (prop2Extend.LocalType == LocalType.Attribute)
                        {
                            var xAttr = ele.Attribute(elName);
                            v = xAttr?.Value;
                        }
                        else
                        {
                            var node = ele.Element(elName);
                            v = node?.Value;
                        }
                        SetTargetValue(target, v, field);
                    }
                }
                arrayAddMethod.Invoke(array, new[] { target });
            }
        }
        internal static void SaveArray(XElement arrayRoot, IEnumerable array, FieldInfo field)
        {
            #region 数据校验
            if (arrayRoot == null) { ConfigLog.Error("写入配置文件中的集合类型,节点对象(arrayRoot)为空."); return; }
            if (array == null) { ConfigLog.Error("写入配置文件中的集合类型,集合对象对象(array)为空."); return; }
            var fieldExtend = field.GetCustomAttribute<ConfigExtend>();
            if (fieldExtend == null) { ConfigLog.Error("写入配置文件中的集合类型,集合的扩展属性(fieldExtend)为空."); return; }
            var fields = fieldExtend.TargetType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            #endregion
            var newElements = new List<XElement>();
            foreach (var target in array)
            {
                var xEle = new XElement(fieldExtend.TargetNode);
                foreach (var f in fields)
                {
                    #region 获取字段的扩展信息
                    var targetExtend = f.GetCustomAttribute<ConfigExtend>() ?? new ConfigExtend
                    {
                        LocalType = LocalType.Attribute,
                        TargetType = field.FieldType,
                        TargetNode = string.Empty
                    };
                    #endregion
                    #region 如果忽略则跳过
                    if (targetExtend.LocalType == LocalType.Ignore) { continue; }
                    #endregion
                    #region 从对象中获取该字段的值
                    var targetValue = f.GetValue(target);
                    #endregion
                    #region 字段名去除开头的 '_' 字符
                    var elName = f.Name;
                    if (string.IsNullOrWhiteSpace(elName)) { continue; }
                    if (elName.StartsWith("_")) { elName = elName.TrimStart('_'); }
                    #endregion
                    if (targetExtend.LocalType == LocalType.Array && !string.IsNullOrWhiteSpace(targetExtend.TargetNode))
                    {
                        var arrayModels = targetValue as IEnumerable;
                        if (arrayModels == null) continue;
                        var subRoot = new XElement(elName);
                        SaveArray(subRoot, arrayModels, f);
                        xEle.Add(subRoot);
                    }
                    else
                    {
                        var v = targetValue?.ToString() ?? string.Empty;
                        if (targetExtend.LocalType == LocalType.Attribute)
                        {
                            xEle.SetAttributeValue(elName, v);
                        }
                        else if (targetExtend.LocalType == LocalType.CData)
                        {
                            var cData = new XCData(v);
                            var cNode = new XElement(elName, cData);
                            cNode.SetAttributeValue("IsCData", bool.TrueString);
                            xEle.Add(cNode);
                        }
                        else
                        {
                            var node = xEle.Element(elName);
                            if (node == null)
                            {
                                var cNode = new XElement(elName) { Value = v };
                                xEle.Add(cNode);
                            }
                            else
                            {
                                node.Value = v;
                            }
                        }
                    }
                }
                newElements.Add(xEle);
            }
            #region 写入集合的Node
            if (newElements.Count <= 0) return;
            arrayRoot.RemoveNodes();
            arrayRoot.Add(newElements);
            #endregion
        }
        internal static void SaveArray(XElement arrayRoot, IEnumerable array, PropertyInfo field)
        {
            #region 数据校验
            if (arrayRoot == null) { ConfigLog.Error("写入配置文件中的集合类型,节点对象(arrayRoot)为空."); return; }
            if (array == null) { ConfigLog.Error("写入配置文件中的集合类型,集合对象对象(array)为空."); return; }
            var fieldExtend = field.GetCustomAttribute<ConfigExtend>();
            if (fieldExtend == null) { ConfigLog.Error("写入配置文件中的集合类型,集合的扩展属性(fieldExtend)为空."); return; }
            var fields = fieldExtend.TargetType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            #endregion
            var newElements = new List<XElement>();
            foreach (var target in array)
            {
                var xEle = new XElement(fieldExtend.TargetNode);
                foreach (var f in fields)
                {
                    #region 获取字段的扩展信息
                    var targetExtend = f.GetCustomAttribute<ConfigExtend>() ?? new ConfigExtend
                    {
                        LocalType = LocalType.Attribute,
                        TargetType = field.PropertyType,
                        TargetNode = string.Empty
                    };
                    #endregion
                    #region 如果忽略则跳过
                    if (targetExtend.LocalType == LocalType.Ignore) { continue; }
                    #endregion
                    #region 从对象中获取该字段的值
                    var targetValue = f.GetValue(target);
                    #endregion
                    #region 字段名去除开头的 '_' 字符
                    var elName = f.Name;
                    if (string.IsNullOrWhiteSpace(elName)) { continue; }
                    if (elName.StartsWith("_")) { elName = elName.TrimStart('_'); }
                    #endregion
                    if (targetExtend.LocalType == LocalType.Array && !string.IsNullOrWhiteSpace(targetExtend.TargetNode))
                    {
                        var arrayModels = targetValue as IEnumerable;
                        if (arrayModels == null) continue;
                        var subRoot = new XElement(elName);
                        SaveArray(subRoot, arrayModels, f);
                        xEle.Add(subRoot);
                    }
                    else
                    {
                        var v = targetValue?.ToString() ?? string.Empty;
                        if (targetExtend.LocalType == LocalType.Attribute)
                        {
                            xEle.SetAttributeValue(elName, v);
                        }
                        else if (targetExtend.LocalType == LocalType.CData)
                        {
                            var cData = new XCData(v);
                            var cNode = new XElement(elName, cData);
                            cNode.SetAttributeValue("IsCData", bool.TrueString);
                            xEle.Add(cNode);
                        }
                        else
                        {
                            var node = xEle.Element(elName);
                            if (node == null)
                            {
                                var cNode = new XElement(elName) { Value = v };
                                xEle.Add(cNode);
                            }
                            else
                            {
                                node.Value = v;
                            }
                        }
                    }
                }
                newElements.Add(xEle);
            }
            #region 写入集合的Node
            if (newElements.Count <= 0) return;
            arrayRoot.RemoveNodes();
            arrayRoot.Add(newElements);
            #endregion
        }

        internal static void SaveObject(XElement objRoot, object obj, ConfigExtend extend)
        {
            #region 数据校验
            if (objRoot == null) { ConfigLog.Error("写入配置文件中的对象类型,节点对象(objRoot)为空."); return; }
            if (obj == null) { ConfigLog.Error("写入配置文件中的对象类型,[对象的引用](obj)为空."); return; }
            var fields = extend.TargetType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            if (fields == null || fields.Length == 0) { ConfigLog.Error("写入配置文件中的对象类型,该对象的fields为空."); return; }
            #endregion
            //var newElement = new XElement(extend.TargetNode);
            foreach (var f in fields)
            {
                #region 获取字段的扩展信息
                var targetExtend = f.GetCustomAttribute<ConfigExtend>() ?? new ConfigExtend
                {
                    LocalType = LocalType.Attribute,
                    TargetType = f.FieldType,
                    TargetNode = string.Empty
                };
                #endregion
                #region 如果忽略则跳过
                if (targetExtend.LocalType == LocalType.Ignore) { continue; }

                #endregion
                #region 从对象中获取该字段的值
                var targetValue = f.GetValue(obj);
                #endregion
                #region 字段名去除开头的 '_' 字符
                var elName = f.Name;
                if (string.IsNullOrWhiteSpace(elName)) { continue; }
                if (elName.StartsWith("_")) { elName = elName.TrimStart('_'); }
                #endregion
                #region 集合类型节点
                if (targetExtend.LocalType == LocalType.Array && !string.IsNullOrWhiteSpace(targetExtend.TargetNode))
                {
                    var arrayModels = targetValue as IEnumerable;
                    if (arrayModels == null) continue;
                    var subRoot = new XElement(elName);
                    SaveArray(subRoot, arrayModels, f);
                    objRoot.Add(subRoot);
                }
                #endregion
                #region 其他类型节点
                else
                {
                    var v = targetValue?.ToString() ?? string.Empty;
                    if (targetExtend.LocalType == LocalType.Attribute)
                    {
                        objRoot.SetAttributeValue(elName, v);
                    }
                    else if (targetExtend.LocalType == LocalType.CData)
                    {
                        var cData = new XCData(v);
                        var cNode = new XElement(elName, cData);
                        cNode.SetAttributeValue("IsCData", bool.TrueString);
                        objRoot.Add(cNode);
                    }
                    else
                    {
                        var node = objRoot.Element(elName);
                        if (node == null)
                        {
                            var cNode = new XElement(elName) { Value = v };
                            objRoot.Add(cNode);
                        }
                        else
                        {
                            node.Value = v;
                        }
                    }
                }
                #endregion
            }
        }
        internal static void LoadObject(XElement objRoot, object obj, ConfigExtend extend)
        {
            #region 数据校验
            if (objRoot == null) { ConfigLog.Error("读取配置文件中的对象类型,节点对象(objRoot)为空."); return; }
            if (obj == null) { ConfigLog.Error("读取配置文件中的对象类型,[对象的引用](obj)为空."); return; }
            var fields = extend.TargetType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            if (fields == null || fields.Length == 0) { ConfigLog.Error("读取配置文件中的对象类型,该对象的fields为空."); return; }
            #endregion
            foreach (var field in fields)
            {
                #region 泛型类型的每个属性的扩展信息 (第二层)
                var prop2Extend = field.GetCustomAttribute<ConfigExtend>() ??
                                  new ConfigExtend
                                  {
                                      LocalType = LocalType.Attribute,
                                      TargetType = field.FieldType,
                                      TargetNode = string.Empty
                                  };
                #endregion
                if (prop2Extend.LocalType == LocalType.Ignore) { continue; }
                var elName = field.Name.TrimStart('_');

                if (prop2Extend.LocalType == LocalType.Array &&
                    !string.IsNullOrWhiteSpace(prop2Extend.TargetNode))
                {
                    var root = objRoot.Element(elName);
                    if (root == null) { continue; }
                    var els = root.Elements(prop2Extend.TargetNode);
                    var arrayFieldAdd = field.FieldType.GetMethod("Add");
                    if (arrayFieldAdd == null) { continue; }
                    var arrayFieldObject = field.GetValue(obj);
                    if (arrayFieldObject == null) { continue; }

                    LoadArray(arrayFieldObject, field, prop2Extend, els);
                }
                else
                {
                    string v;
                    if (prop2Extend.LocalType == LocalType.Attribute)
                    {
                        var xAttr = objRoot.Attribute(elName);
                        v = xAttr?.Value;
                    }
                    else
                    {
                        var node = objRoot.Element(elName);
                        v = node?.Value;
                    }
                    SetTargetValue(obj, v, field);
                }
            }
        }

        /// <summary>
        /// 获取异常的内部消息
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        internal static string InnerMessager(this Exception e)
        {
            return e.InnerException == null
                ? e.Message
                : e.InnerException.InnerException?.Message ?? e.InnerException.Message;
        }
    }

}
