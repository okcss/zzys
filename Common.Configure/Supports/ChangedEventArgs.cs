﻿using System;

namespace Common.Configure.Supports
{
    [Serializable]
    public class ChangedEventArgs : EventArgs
    {
        /// <summary>
        /// 配置文件名称
        /// </summary>
        public string ConfigName { get; private set; }
        /// <summary>
        /// 属性名称
        /// </summary>
        public string PropertyName { get; private set; }
        /// <summary>
        /// 属性的原值
        /// </summary>
        public dynamic OldValue { get; private set; }
        /// <summary>
        /// 属性的新值
        /// </summary>
        public dynamic NewValue { get; private set; }


        /// <inheritdoc />
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="configName">配置文件名称</param>
        /// <param name="propName">变更的属性名</param>
        /// <param name="newValue">属性的新值</param>
        /// <param name="oldValue">属性的原值</param>
        public ChangedEventArgs(string configName, string propName, dynamic oldValue, dynamic newValue)
        {
            ConfigName = configName;
            PropertyName = propName;
            OldValue = oldValue;
            NewValue = newValue;
        }
    }
}
