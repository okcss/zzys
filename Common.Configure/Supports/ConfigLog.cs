﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Common.Configure.Supports
{
    internal static class ConfigLog
    {
        private static readonly object Lock = new object();

        private static bool IsHtml = false;

        private static StreamWriter CreateFile()
        {
            try
            {
                var basePath = ConfigStatic.LogRoot;
                if (!Directory.Exists(basePath)) Directory.CreateDirectory(basePath);

                var wirter = new StreamWriter(Path.Combine(basePath, FileName), true) { AutoFlush = true };

                return wirter;
            }
            catch (Exception)
            {
                var basePath = Path.Combine(@"C:\", ConfigStatic.LogRoot);
                if (!Directory.Exists(basePath)) Directory.CreateDirectory(basePath);

                var wirter = new StreamWriter(Path.Combine(basePath, FileName), true) { AutoFlush = true };

                return wirter;
            }
        }
        /// <summary>
        /// 记录错误日志
        /// </summary>
        /// <param name="msg"></param>
        internal static void Error(string msg)
        {
            Writer(msg, EventLogEntryType.Error);
        }

        private static void Writer(string msg, EventLogEntryType level = EventLogEntryType.Information)
        {
            lock (Lock)
            {
                using (var wirter = CreateFile())
                {
                    var line = string.Format(IsHtml ? "<p style='Color:green;font-size:13px;Width:100%;Display:block;'>{0}-><span style='color:red;'>{1}</span><span style='color:black;'>{2}</span></p>" : "{0}->{1}：{2}", DateTime.Now, LevelName(level), msg);
                    wirter.WriteLine(line);
                }
            }
        }

        internal static string GetInner(Exception ex)
        {
            var lastEx = ex.InnerException;
            var index = 1;
            var error = new StringBuilder("Inner 信息：");
            while (lastEx != null)
            {
                error.Append($"Inner{index}：{lastEx.Message}；");
                index++;
                lastEx = lastEx.InnerException;
            }
            return error.ToString();
        }

        private static string LevelName(EventLogEntryType level)
        {
            switch (level)
            {
                case EventLogEntryType.Error:
                    return "错误";
                case EventLogEntryType.FailureAudit:
                    return "失败";
                case EventLogEntryType.Information:
                    return "信息";
                case EventLogEntryType.SuccessAudit:
                    return "成功";
                case EventLogEntryType.Warning:
                    return "警告";
                default:
                    return "未知";
            }
        }

        private static string FileName
        {
            get
            {
                return $"{DateTime.Now:yyyy.MM.dd}{(IsHtml ? ".html" : ".txt")}";
            }
        }
    }
}
